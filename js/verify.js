$(document).ready(function() {
    // Generate a simple captcha
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));

    $('#defaultForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            lname: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            cname: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'The company name is required and cannot be empty'
                    }
                }
            },
            residence: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Residence Address is required and cannot be empty'
                    }
                }
            },
            city: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'City is required and cannot be empty'
                    }
                }
            },
            distt: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Distt. is required and cannot be empty'
                    }
                }
            },
            state: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'State is required and cannot be empty'
                    }
                }
            },
            country: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Country is required and cannot be empty'
                    }
                }
            },
            marital: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Matrimonial Status is required and cannot be empty'
                    }
                }
            },
            occu: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Occupation is required and cannot be empty'
                    }
                }
            },
            edu: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Education is required and cannot be empty'
                    }
                }
            },
            cv: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Curriculum Vitae is required and cannot be empty'
                    }
                }
            },
            acc_name: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Account holder name is required and cannot be empty'
                    }
                }
            },
            bank_name: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Bank name is required and cannot be empty'
                    }
                }
            },
            ifsc: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'IFSC code is required and cannot be empty'
                    }
                }
            },
            bank_add: {
                group: '.col-lg-4',
                validators: {
                    notEmpty: {
                        message: 'Bank address is required and cannot be empty'
                    }
                }
            },
            
            email: {
                validators: {
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            
            dob: {
                validators: {
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'date of birth is not valid'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
        }
    });

    // Validate the form manually
    $('#validateBtn').click(function() {
        $('#defaultForm').bootstrapValidator('validate');
    });

    $('#resetBtn').click(function() {
        $('#defaultForm').data('bootstrapValidator').resetForm(true);
    });
});