<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Food & Beverages</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Industries</a></li>
						<li><a href="#">Food & Beverages</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="single-project-content">
								<img alt="" src="upload/single-project.jpg">
								<h2>Food & Beverages</h2>
								<p>Unlike many other industries, F&B is an industry that most consumers patronize on a regular basis. With all of those available currency, competition has become fierce. Brand promise and differentiation, whether it be low price, product selection or speed and convenience, is critical to success in an oversaturated market.<br>

AUDIT DADDY understands your unique challenge of earning the loyalty of every customer while successfully differentiating yourself in a hyper-competitive market, all the while working with multiple locations and a massive number of fast-turning SKUs.</p>
								<h4>Bring on your Brand</h4>
								<p> Assess operational performance in areas critical to your brand promise and to customer needs such as speed of check out, cleanliness, employee helpfulness and more with Mystery Shopping

								<br>- Ensure accurate and critical product availability, selection, pricing, speed-to-shelf and display/promotion with Retail Audits and competitive pricing
                                   <br>- Understand critical customer attitudes and perceptions on items such as product selection, product quality and overall satisfaction with Customer Satisfaction Information

                                   <br>- Understand what operational compliance areas matter the most to your sales and where. Identify which of your customers want what from their Food and Beverages store visits based on our unique segmentation analytics. Insights like these with ground-breaking Advanced Analytics such as predictive statistical models
                                   <br>- Put all of this into action with AuditDADDY, a completely integratedcustomer intelligence technology platform that pushes results down to the store level for maximum accountability
     </p>
							</div>
						</div>

						
					</div>

					<div class="latest-projects">
						<h3>Latest Projects</h3>
						<div class="row">
							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image1.jpg">
										<div class="hover-box">
											<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Attractive Admin UI</h2>
										<span>short description of portfolio item</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image2.jpg">
										<div class="hover-box">
											<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Green Leaf Falling Dawn</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image3.jpg">
										<div class="hover-box">
											<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Beautiful Wallpaper</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

						</div>
					</div>
											
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
	<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>