		<!-- End Header -->
<?php
	include 'header.php'; 
?>	
		<!-- slider 
			================================================== -->
		<div id="slider">
			<!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

			<div class="fullwidthbanner-container">
				<div class="fullwidthbanner">
					<ul>
						<!-- THE FIRST SLIDE -->
						<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/01.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption large_text sfb"
								 data-x="15"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Welcome to Audit Daddy</div>

							<div class="caption big_white sft stt"
								 data-x="15"
								 data-y="147"
								 data-speed="500"
								 data-start="1400"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >WE ARE FOR ALL  <span></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="15"
								 data-y="194"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" >CORPORATE/ BUISNESS/ GLOBAL, <span> </span> </div>

							<div class="caption small_text sft stt"
								 data-x="15"
								 data-y="225"
								 data-speed="500"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >LET'S MOVE ON</div>

							<div class="caption randomrotate"
								 data-x="15"
								 data-y="285"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon1.png" alt="Image 1"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="31"
								 data-y="390"
								 data-speed="600"
								 data-start="2500"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" >Hospitality <br> Restaurant</div>

							<div class="caption randomrotate"
								 data-x="125"
								 data-y="285"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon2.png" alt="Image 2"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="141"
								 data-y="390"
								 data-speed="600"
								 data-start="2900"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" >  Apparel<br> Industry</div>

							<div class="caption randomrotate"
								 data-x="235"
								 data-y="285"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon3.png" alt="Image 3"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="251"
								 data-y="390"
								 data-speed="600"
								 data-start="3300"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" >Gas<br>  Station</div>

							<div class="caption randomrotate"
								 data-x="345"
								 data-y="285"
								 data-speed="600"
								 data-start="3500"
								 data-easing="easeOutExpo" data-end="7650" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon4.png" alt="Image 4"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="355"
								 data-y="390"
								 data-speed="600"
								 data-start="3700"
								 data-easing="easeOutExpo" data-end="7700" data-endspeed="300" data-endeasing="easeInSine" >Bank<br> Industry</div>

							<div class="caption randomrotate"
								 data-x="795"
								 data-y="32"
								 data-speed="600"
								 data-start="4000"
								 data-easing="easeOutExpo" data-end="7800" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/profile.png" alt="Image 6"></div>
						</li>
						<!-- THE Second SLIDE -->
						<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/04.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption large_text sfb"
								 data-x="15"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Welcome to Audit Daddy</div>

							<div class="caption big_white sft stt"
								 data-x="15"
								 data-y="147"
								 data-speed="500"
								 data-start="1400"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >WE ARE FOR ALL  <span></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="15"
								 data-y="194"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" >CORPORATE/ BUISNESS/ GLOBAL, <span> </span> </div>

							<div class="caption small_text sft stt"
								 data-x="15"
								 data-y="225"
								 data-speed="500"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >LET'S MOVE ON</div>

							<div class="caption randomrotate"
								 data-x="15"
								 data-y="285"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon1.png" alt="Image 1"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="31"
								 data-y="390"
								 data-speed="600"
								 data-start="2500"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" >Automobile <br> Industry</div>

							<div class="caption randomrotate"
								 data-x="125"
								 data-y="285"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon2.png" alt="Image 2"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="141"
								 data-y="390"
								 data-speed="600"
								 data-start="2900"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" >   Consumer<br> Durable</div>

							<div class="caption randomrotate"
								 data-x="235"
								 data-y="285"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon3.png" alt="Image 3"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="251"
								 data-y="390"
								 data-speed="600"
								 data-start="3300"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" >Super<br>  Mart</div>

							<div class="caption randomrotate"
								 data-x="345"
								 data-y="285"
								 data-speed="600"
								 data-start="3500"
								 data-easing="easeOutExpo" data-end="7650" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon4.png" alt="Image 4"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="355"
								 data-y="390"
								 data-speed="600"
								 data-start="3700"
								 data-easing="easeOutExpo" data-end="7700" data-endspeed="300" data-endeasing="easeInSine" >IT<br> Industry</div>

							<div class="caption randomrotate"
								 data-x="795"
								 data-y="32"
								 data-speed="600"
								 data-start="4000"
								 data-easing="easeOutExpo" data-end="7800" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/profile.png" alt="Image 6"></div>
						</li>

						<!-- THE third SLIDE -->
					<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/03.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption large_text sfb"
								 data-x="15"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Welcome to Audit Daddy</div>

							<div class="caption big_white sft stt"
								 data-x="15"
								 data-y="147"
								 data-speed="500"
								 data-start="1400"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >WE ARE FOR ALL  <span></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="15"
								 data-y="194"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" >CORPORATE/ BUISNESS/ GLOBAL, <span> </span> </div>

							<div class="caption small_text sft stt"
								 data-x="15"
								 data-y="225"
								 data-speed="500"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >LET'S MOVE ON</div>

							<div class="caption randomrotate"
								 data-x="15"
								 data-y="285"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon1.png" alt="Image 1"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="31"
								 data-y="390"
								 data-speed="600"
								 data-start="2500"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" >Telecom <br> Industry</div>

							<div class="caption randomrotate"
								 data-x="125"
								 data-y="285"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon2.png" alt="Image 2"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="141"
								 data-y="390"
								 data-speed="600"
								 data-start="2900"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" >  Food & Beverages<br> Industry</div>

							<div class="caption randomrotate"
								 data-x="235"
								 data-y="285"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon3.png" alt="Image 3"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="251"
								 data-y="390"
								 data-speed="600"
								 data-start="3300"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" >Transportation<br>  Industry</div>

						<!--	<div class="caption randomrotate"
								 data-x="345"
								 data-y="285"
								 data-speed="600"
								 data-start="3500"
								 data-easing="easeOutExpo" data-end="7650" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon4.png" alt="Image 4"></div>

							 <div class="caption modern_small_text_dark sft"
								 data-x="355"
								 data-y="390"
								 data-speed="600"
								 data-start="3700"
								 data-easing="easeOutExpo" data-end="7700" data-endspeed="300" data-endeasing="easeInSine" >Automobile<br> Industry</div>

							<div class="caption randomrotate"
								 data-x="795"
								 data-y="32"
								 data-speed="600"
								 data-start="4000"
								 data-easing="easeOutExpo" data-end="7800" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/profile.png" alt="Image 6"></div>
					 -->	</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End slider -->

		<!-- content 
			================================================== -->
		<div class="padding10">
		<div class="row mar10 padding10" >
              <h2>Industries</h2>
			  <p>Retail sweating the small things across the world??? We can help. 

<br>For retailers of the various brands across verticals, the seemingly smallest aspects of the in-store experience can mean millions in annual sales across many locations. We serve the following the industries today... 
</p>
</div>
			  <div class="row padding10">
			     <div class="col-sm-6">
				    <img class="img-responsive" src="upload/it.jpg">
					<p class="mar10">Assess operational performance and brand compliance in areas such as 
sales person product knowledge, 
grooming & Hygiene check, 
up-selling Skills
Communication skills

Check out with our mystery shopping. </p>
				 
				 </div>
			   <div class="col-sm-6">
				    <img class="img-responsive" src="upload/beverage.jpg">
					<p class="mar10">Monitor in-store realities such 
as product availability, 
pricing accuracy, 
promotional materials display and 
how your pricing compares to your competitors with retail audits. </p>
				 
				 </div>
			  
			  </div>
			  
			  <div class="row padding10">
			     <div class="col-sm-6">
				    <img class="img-responsive" src="upload/bank.jpg">
					<p class="mar10">Understand customer attitudes and perceptions on topics such as 
store layout, 
product selection, 
sales person approach, and 
customer service with customer satisfaction solutions. </p>
				 
				 </div>
			   <div class="col-sm-6">
				    <img class="img-responsive" src="upload/gas.jpg">
					<p class="mar10">Use AuditDADDY to help you execute 
promotions and seasonal floor sets correctly and on time with merchandising services.
Understand what operational compliance areas matter the most to your sales. 
Identify what your customers want from their experience based on our unique segmentation analytics. 
Grow your business using insights like these with ground-breaking advanced analytics and predictive statistical models. </p>
				 
				 </div>
			  
			  </div>
			
			


			

			

			

			

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
			<?php include'footer.php';
?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
	<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:500,

					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off


					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


					fullWidth:"on",

					shadow:1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});


				// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
				// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
					api.bind("revolution.slide.onloaded",function (e) {


						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);

							var timer = setInterval(function() {

								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})

						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
				// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
			});
	</script>
	<script>
		jQuery(function(){ 
			DevSolutionSkill.init('circle'); 
			DevSolutionSkill.init('circle2'); 
			DevSolutionSkill.init('circle3'); 
			DevSolutionSkill.init('circle4'); 
			DevSolutionSkill.init('circle5'); 
			DevSolutionSkill.init('circle6');
		});
	</script>
</body>

</html>