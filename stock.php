<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Stock Audit</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Stock Audit</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="blog-box with-sidebar">
				<div class="container">
					<div class="row">

						<div class="col-md-9 single-post">
							<div class="single-post-content">
								<img alt="" src="upload/stock.jpg">
								<h2>Stock Audit</h2><hr>
								<p>Stock audit is the key areas of expertise and competence for our Audit Services. Stocks and physical assets such as machinery, office equipment, IT systems etc. are value assets of a company. With companies today operating across multiple locations with various channel partners, ensuring all assets exist as per the books of record is a challenge for the operations and the facilities functions.
<br><br/>
AuditDADDY provides Stock audit in India to companies helping them safeguard and monitor their physical assets and inventories efficiently. AuditDADDY Stock audit team follows a strict audit and reporting mechanism that ensures that every aspect of stock is evaluated and findings are reported in a transparent manner to levels concerned. <br>

AuditDADDY all India network of branches and full-time trained employees gives it the advantage of local presence, which translates into quicker audits at lower costs for clients.<br>

<br/>AuditDADDY Stock Audit conducts a comprehensive and accurate valuation of inventories, by taking into account physical controls, obsolete inventory, scrap and returned goods. Under the audit, records of inward and outward movement of goods and stock procedures on the shop floor are verified thoroughly.

</p>
								
								
							</div>

							

							
						</div>

					<div class="col-md-3 sidebar">
							<h3>Send us a message</h3>
							<form id="contact-form" method = "POST" action="sendUs.php">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

								
									
									<div class="float-input">
									<input name="phone" id="phone" type="text" placeholder="Phone No">
										<span><i class="fa fa-phone"></i></span>
									</div>
									
										
								</div>
						<div class="float-input3">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span></span>
									</div>
								<div class="textarea-input">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div id="msg" class="message"></div>
								<input type="submit" id="submit_contact" value="Send Message">

							</form>
						</div>
					</div>
							
							
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>