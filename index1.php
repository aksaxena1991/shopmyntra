		<!-- End Header -->
<?php
	include 'header.php'; 
?>	
		<!-- slider 
			================================================== -->
		<div id="slider">
			<!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

			<div class="fullwidthbanner-container">
				<div class="fullwidthbanner">
					<ul>
						<!-- THE FIRST SLIDE -->
						<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/t2.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption large_text sfb"
								 data-x="15"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Welcome to AUDIT DADDY</div>

							<div class="caption big_white sft stt"
								 data-x="15"
								 data-y="147"
								 data-speed="500"
								 data-start="1400"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >  <span></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="15"
								 data-y="194"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" > <span> </span> </div>

							<div class="caption small_text sft stt"
								 data-x="15"
								 data-y="225"
								 data-speed="500"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >LET'S MOVE ON</div>

							<div class="caption randomrotate"
								 data-x="15"
								 data-y="285"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon1.png" alt="Image 1"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="31"
								 data-y="390"
								 data-speed="600"
								 data-start="2500"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" >Services <br> </div>

							<div class="caption randomrotate"
								 data-x="125"
								 data-y="285"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon2.png" alt="Image 2"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="141"
								 data-y="390"
								 data-speed="600"
								 data-start="2900"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" >Industries<br> </div>

							<div class="caption randomrotate"
								 data-x="235"
								 data-y="285"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon3.png" alt="Image 3"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="251"
								 data-y="390"
								 data-speed="600"
								 data-start="3300"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" >Technology<br>  </div>

							<div class="caption randomrotate"
								 data-x="345"
								 data-y="285"
								 data-speed="600"
								 data-start="3500"
								 data-easing="easeOutExpo" data-end="7650" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon4.png" alt="Image 4"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="370"
								 data-y="390"
								 data-speed="600"
								 data-start="3700"
								 data-easing="easeOutExpo" data-end="7700" data-endspeed="300" data-endeasing="easeInSine" >  Audits<br> </div>

							<div class="caption randomrotate"
								 data-x="795"
								 data-y="32"
								 data-speed="600"
								 data-start="4000"
								 data-easing="easeOutExpo" data-end="7800" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/profile.png" alt="Image 6"></div>
						</li>
						<!-- THE Second SLIDE -->
						<li data-transition="papercut" data-slotamount="15" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE second SLIDE -->
							<img alt="" src="upload/slider-revolution/td2.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->

							<div class="caption big_white sft stt"
								 data-x="295"
								 data-y="116"
								 data-speed="500"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style"></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="-20"
								 data-y="180"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style2" style="font-size:22px;"><ul class="banner2"><li> > Mystery Shopping</li>
                                        <li> > Stock Audits</li>
										<li> > Customer Surveys</li>
										<li> > MOP Audit</li>
                     				</ul>
									</div>

							<div class="caption randomrotate"
								 data-x="137"
								 data-y="100"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><h3 style="font-size:35px;">Customer Experience </h3></div>
							
							
							<!-- 

							<div class="caption randomrotate"
								 data-x="482"
								 data-y="227"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/ipad2.png" alt="Image 2"></div>

							<div class="caption randomrotate"
								 data-x="761"
								 data-y="282"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/ipad3.png" alt="Image 3"></div> -->
						</li>

						<!-- THE third SLIDE -->
						<li data-transition="turnoff" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE second SLIDE -->
							<img alt="" src="upload/slider-revolution/3.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->

							<div class="caption randomrotate"
								 data-x="100"
								 data-y="122"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/slide.png" alt="Image 1"></div>

							<div class="caption big_white sft stt slide3-style"
								 data-x="520"
								 data-y="194"
								 data-speed="500"
								 data-start="1500"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >Be a part of<span>AUDIT DADDY </span> </div>

							<div class="caption modern_medium_fat sft stt slide3-style"
								 data-x="520"
								 data-y="237"
								 data-speed="500"
								 data-start="1700"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style2">Provides </span> reasonable assurance</div>

							<div class="caption lfr medium_text"
								 data-x="520"
								 data-y="278"
								 data-speed="600"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" ><a href="register.php">Register</a></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End slider -->

		<!-- content 
			================================================== -->
		<div id="content">

			<div class="fullwidth-box">
				<div class="container"  style="margin-bottom: 30px;">
				
					<!-- services-box -->
					<div class="services-box">
						<div class="row">
							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon1" href="#"><i class="fa fa-cogs"></i></a>
									<div class="services-post-content">
										<h4><b>We Love Audit</b> </h4>
										<p class="pull-left">We are a leading audit agency in India, serving global brands in the field of Mystery Shopping, Retail and service audits. AUDIT DADDY leverages its rich pedigree of founders which have been the pioneers of Global IT Industry coming from premier institutes.<br/>  As our client base continues to grow, our customers include companies from Apparel, Consumer Durables, IT, Telecom and FMCG industry. 
										<img class="img-responsive text-center" src="images/audit1.jpg" style="margin-left:80px;"> </p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon2" href="#"><i class="fa fa-desktop"></i></a>
									<div class="services-post-content">
										<h4><b>Why Work with Us</b></h4>
										<p class="pull-left">We are a team of hardworking committed professionals comprises of 10,000+ Auditors on a pan India basis supported by our technology and operations team. 
<br/><br/>Advantages with AUDIT DADDY

<br>
1. Pan India coverage <br>
2. One shop window to get various kinds of audit services like mystery shopping, display audits, stock audits, call centre audits and so on.<br>
3. End-to-End account ownership from strategic consulting to last mile audit execution<br> 
4. Audits are executed with integration of mobile technology<br> 
5. Comprehensive audit analytics wing.

</p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon3" href="#"><i class="fa fa-book"></i></a>
									<div class="services-post-content">
										<h4><b>Our Values & Mission </b></h4>
										<p class="pull-left"> At AUDIT DADDY our sole mission is helping our customers graduate to the highest level in retail hygiene by sharing audit feedbacks. In this endeavour, execution of high quality fair practices by integrating state of the art technology to eliminate the best possible human interferences.
									   <br>Our purpose is to partner and contribute for the long term growth of clients with following principles:
									   <br>•	To understand the pain areas of the clients
                                       <br>•	On Field research to find solutions
                                       <br>    •	Invest in training the resources to deliver qualitative results.
                                          <br> •	Collaborating with the client to formulate the last mile objectives.
                                           <br>•	Ensuring continuous deep client relationships and services with value
.
									   
									   
									   </p>
									
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- faqs-box -->
					
					</div>
                       <!--<div class="fullwidth-box infographic-box">
				<div class="container">
				
					<h3><span>Infographic elements</span></h3>

					<ul class="gender-list">
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-male"></i></a></li>
						<li><a href="#"><i class="fa fa-female"></i></a></li>
						<li><a href="#"><i class="fa fa-female"></i></a></li>
						<li><a href="#"><i class="fa fa-female"></i></a></li>
						<li><a href="#"><i class="fa fa-female"></i></a></li>
						<li><a href="#"><i class="fa fa-female"></i></a></li>
					</ul>

					<div class="skills-bar">
						<div class="row">
							<div class="col-md-3">
								<div id="circle" data-percent="80"></div>
								<p> Trusted Employees </p>
							</div>
							<div class="col-md-3">
								<div id="circle2" data-percent="60"></div>
								<p> Successful Audits</p>
							</div>
							<div class="col-md-3">
								<div id="circle3" data-percent="90"></div>
								<p>Positive Serving Outlets </p>
							</div>
							<div class="col-md-3">
								<div id="circle4" data-percent="70"></div>
								<p>Return Clients </p>
							</div>
							
						</div>
					</div>

				</div>
			</div>
					<!-- recent-works-box -->
					<div class="recent-works" style="padding:10px;">
						<h3>Services</h3>
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

							<!-- Wrapper for slides -->
							<div class="carousel-inner">

								<div class="item active">
									<div class="row">

									<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="onsite.php"><img alt="" src="images/s6.jpg"></a>
													<!-- <div class="hover-box">
														<a class="zoom" href="upload/image4.jpg"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div> -->
												</div>
												<div class="work-post-content">
													<h2> On Site Support Audit</h2>
													<span></span>
												</div>										
											</div>
										</div>

										
										
										<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="call.php"><img alt="" src="images/s4.jpg"></a>
													<!--<div class="hover-box">
														<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div>-->
												</div>
												<div class="work-post-content">
													<h2> Call center Audit</h2>
													<span></span>
												</div>										
											</div>
										</div>

										<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="stock.php"><img alt="" src="images/s3.jpg"></a>
													<!--<div class="hover-box">
														<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div>-->
												</div>
												<div class="work-post-content">
													<h2>    Stock Audit</h2>
													<span></span>
												</div>										
											</div>
										</div>
									</div>
								</div>

								<div class="item">
									<div class="row">
										

										<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="mop.php"><img alt="" src="images/s5.jpg"></a>
													<!--<div class="hover-box">
														<a class="zoom" href="upload/image3.jpg"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div>-->
												</div>
												<div class="work-post-content">
													<h2>MOP Audit</h2>
													<span></span>
												</div>										
											</div>
										</div>
										
											<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="mystery.php"><img alt="" src="images/s7.jpg"></a>
													<!--<div class="hover-box">
														<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div> -->
												</div>
												<div class="work-post-content">
													<h2>  Mystery Shopping</h2>
													<span></span>
												</div>
											</div>
										</div>

										<div class="col-md-4">
											<div class="work-post">
												<div class="work-post-gal">
													<a href="display.php"><img alt="" src="http://trionprojects.com/auditdaddy/upload/display.jpg" style="height: 264px;width: auto"></a>
													<!--<div class="hover-box">
														<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
														<a class="page" href="#"><i class="fa fa-arrow-circle-right"></i></a>
													</div>-->
												</div>
												<div class="work-post-content">
													<h2>Display Audit</h2>
													<span></span>
												</div>										
											</div>
										</div>
										

									</div>

								</div>

							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"></a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next"></a>
						</div>
					</div>
					
				</div>
			


			<!-- client-testimonials -->
			

			<!-- staff-box -->
	<!--	<div class="fullwidth-box staff-box">
				<h3><span>Our Team Members</span></h3>
				<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">

				
					<div class="carousel-inner">

						<div class="item active">
							<div class="container">
								<div class="row">
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team1.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team2.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team3.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team4.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="container">
								<div class="row">
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team1.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team2.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team3.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="staff-post">
											<div class="staff-post-gal">
												<img alt="" src="upload/team4.jpg">
											</div>
											<div class="staff-post-content">
												<h5>Owen Miller</h5>
												<span>manager</span>
												<ul class="staf-social">
													<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
													<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
													<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
													<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
													<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
													<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					
					<a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
			</div> -->

			

			

			

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
			<?php include'footer.php';
?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
	<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:500,

					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off


					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


					fullWidth:"on",

					shadow:1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});


				// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
				// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
					api.bind("revolution.slide.onloaded",function (e) {


						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);

							var timer = setInterval(function() {

								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})

						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
				// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
			});
	</script>
	<script>
		jQuery(function(){ 
			DevSolutionSkill.init('circle'); 
			DevSolutionSkill.init('circle2'); 
			DevSolutionSkill.init('circle3'); 
			DevSolutionSkill.init('circle4'); 
			DevSolutionSkill.init('circle5'); 
			DevSolutionSkill.init('circle6');
		});
	</script>
</body>

</html>