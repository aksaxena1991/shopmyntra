<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Display Audit</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Display Audit</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="blog-box with-sidebar">
				<div class="container">
					<div class="row">

						<div class="col-md-9 single-post">
							<div class="single-post-content">
								<img alt="" src="upload/display.jpg">
								<h2>Brand Visibly Audit </h2><hr>
								<p><b>AuditDADDY delivers your brand visibility Index, almost real time, online
Drill down from country to store level, with online visuals. Do a virtual market visit.
</b></p>
							<br/>	<p>Assess Range, Prominence, Purity and Depth

Whether you are an exclusive company showroom or for products placed in a large retail format supermarket, AUDIT DADDY enables you with an insight of the display quality of brand assets and mass activations campaigns.

</p>
							</div>

							

							
						</div>
							
							
							
							<div class="col-md-3 sidebar">
							<h3>Send us a message</h3>
							<form id="contact-form" method = "POST" action="sendUs.php">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

								
									
									<div class="float-input">
									<input name="phone" id="phone" type="text" placeholder="Phone No">
										<span><i class="fa fa-phone"></i></span>
									</div>
									
										
								</div>
						<div class="float-input3">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span></span>
									</div>
								<div class="textarea-input">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div id="msg" class="message"></div>
								<input type="submit" id="submit_contact" value="Send Message">

							</form>
						</div>
					</div>
						

					
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>