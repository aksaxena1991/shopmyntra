<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>POS Staff Audit</h2>
					<ul class="page-tree">
						<li><a href="#">Services</a></li>
						<li><a href="#">POS Staff Audit</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="blog-box with-sidebar">
				<div class="container">
					<div class="row">

						<div class="col-md-9 single-post">
							<div class="single-post-content">
								<img alt="" src="upload/pos.jpg">
								<h2>Pos Audit</h2><hr>
								<p>We measure what matters in your customer experienc
Our evaluation tools allow you to measure areas of your customer service experience that are most likely to drive sales and customer satisfaction. These are including but not limited to
 </p>
<h4>In shop Promoters</h4>
<hr>
<p><b>1.	Staff friendliness<br>
2.	Number of staff touch points throughout your location<br>
3.	Sales techniques<br>
4.	Product knowledge<br>
5.	Communication Skills<br>
6.	Wait and service times</b></p><br/>
<h4>Your Store Site</h4><hr>
 <p><b>Site cleanliness<br>
 Store layout<br>
 Merchandising<br>
 Promotional efforts</b></p>

								
							</div>

							

							
						</div>

					
								

				<div class="col-md-3 sidebar">
							<h3>Send us a message</h3>
							<form id="contact-form" method = "POST" action="sendUs.php">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

								
									
									<div class="float-input">
									<input name="phone" id="phone" type="text" placeholder="Phone No">
										<span><i class="fa fa-phone"></i></span>
									</div>
									
										
								</div>
						<div class="float-input3">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span></span>
									</div>
								<div class="textarea-input">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div id="msg" class="message"></div>
								<input type="submit" id="submit_contact" value="Send Message">

							</form>
						</div>
					</div>
							

							<div class="leave-comment">
								<h3>Leave a comment</h3>
								<form class="comment-form">
									<div class="row">

										<div class="col-md-4">
											<input type="text" name="name" placeholder="name"/>	
											<input type="text" name="email" placeholder="e-mail"/>	
											<input type="text" name="website" placeholder="website"/>	
										</div>

										<div class="col-md-8">
											<textarea placeholder="message"></textarea>
											<input type="submit" value="Add a Comment"/>
										</div>

									</div>
								</form>
							</div>
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>