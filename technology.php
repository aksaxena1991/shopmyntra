		<!-- End Header -->
<?php
	include 'header.php'; 
?>	
		<!-- slider 
			================================================== -->
		<div id="slider">
			<!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

			<div class="fullwidthbanner-container">
				<div class="fullwidthbanner">
					<ul>
						<!-- THE FIRST SLIDE -->
						<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/03.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption large_text sfb"
								 data-x="15"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Technology Automation</div>

							<div class="caption big_white sft stt"
								 data-x="15"
								 data-y="147"
								 data-speed="500"
								 data-start="1400"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >  <span></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="15"
								 data-y="194"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" > <span> </span> </div>

							<div class="caption small_text sft stt"
								 data-x="15"
								 data-y="225"
								 data-speed="500"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >LET'S MOVE ON</div>

							<div class="caption randomrotate"
								 data-x="15"
								 data-y="285"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon1.png" alt="Image 1"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="31"
								 data-y="390"
								 data-speed="600"
								 data-start="2500"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" >Automated  <br> Processes</div>

							<div class="caption randomrotate"
								 data-x="125"
								 data-y="285"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon2.png" alt="Image 2"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="141"
								 data-y="390"
								 data-speed="600"
								 data-start="2900"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" >Real Time<br> Information  </div>

							<div class="caption randomrotate"
								 data-x="235"
								 data-y="285"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon3.png" alt="Image 3"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="251"
								 data-y="390"
								 data-speed="600"
								 data-start="3300"
								 data-easing="easeOutExpo" data-end="7600" data-endspeed="300" data-endeasing="easeInSine" >GEO Tagged <br>  Reports</div>

							<div class="caption randomrotate"
								 data-x="345"
								 data-y="285"
								 data-speed="600"
								 data-start="3500"
								 data-easing="easeOutExpo" data-end="7650" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/icon4.png" alt="Image 4"></div>

							<div class="caption modern_small_text_dark sft"
								 data-x="355"
								 data-y="390"
								 data-speed="600"
								 data-start="3700"
								 data-easing="easeOutExpo" data-end="7700" data-endspeed="300" data-endeasing="easeInSine" >Actionable & <br> Insightful Reports </div>

			<!--				<div class="caption randomrotate"
								 data-x="795"
								 data-y="32"
								 data-speed="600"
								 data-start="4000"
								 data-easing="easeOutExpo" data-end="7800" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/phn1.png" alt="Image 6"></div>
			-->			</li>
						<!-- THE Second SLIDE -->
							<li data-transition="papercut" data-slotamount="15" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE second SLIDE -->
						<img alt="" src="upload/slider-revolution/tech1.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->

							<div class="caption big_white sft stt"
								 data-x="295"
								 data-y="116"
								 data-speed="500"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style"></span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="97"
								 data-y="100"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style2" style="font-size:35px">Technology Automation<br/><br/><br/></div>
			<div class="caption randomrotate"
								 data-x="97"
								 data-y="182"
								 data-speed="600"
								 data-start="2200"
								 data-easing="easeOutExpo" data-end="7350" data-endspeed="300" data-endeasing="easeInSine" >
								 <ul class="banner1">
								 
								 <li>> Automated Processes</li> 
								 <li>> Real Time, Validated & measurable<br> Information</li> 
								 <li>> GEO Tagged Reports</li> 
								 <li>> Actionable & Insightful Reports</li> 
								 </ul>
								 </div>
								 

							<!--<div class="caption randomrotate"
								 data-x="482"
								 data-y="227"
								 data-speed="600"
								 data-start="2700"
								 data-easing="easeOutExpo" data-end="7450" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/ipad2.png" alt="Image 2"></div>

						<div class="caption randomrotate"
								 data-x="761"
								 data-y="282"
								 data-speed="600"
								 data-start="3100"
								 data-easing="easeOutExpo" data-end="7550" data-endspeed="300" data-endeasing="easeInSine" ><img src="images/slider-icons/ipad3.png" alt="Image 3"></div>
	-->				</li>

						<!-- THE third SLIDE -->
					
					</ul>
				</div>
			</div>
		</div>
		<!-- End slider -->

		<!-- content 
			================================================== -->
		<!-- <div class="padding10"> -->
		<div>
		<div class="row mar10 padding10" >
		   <div class="col-sm-8">
              <h2>AuditDADDY Automation</h2><br/>
			  <p>Product availability, employee up-selling, verbal promotion reminders, promotional signage and more, all hold insurmountable revenue potential. From high-end specialty brands and big box value chains to department stores, convenience stores and gas stations, it can be particularly challenging for retailers today to juggle, drive and prioritize this endless list of performance areas. 

                 <br/> <br>That’s why AUDIT DADDY provides not only the data you need to monitor these areas but also the analytics and technology platform you need to prioritize and improve on them to predictably drive same-store sales across your organization. The Science of the Sale:

			  </p>
</div>
<div class="col-sm-4">

	<div class="row">
			<div class="col-sm-4">
			
								<ul class="flickr-list">
									<li><img alt="" src="images/f1.png"></li>
									
								</ul>
		
		</div>
		<div class="col-sm-8">
		<h4 class="font800">Automated Processes</h4>
	</div>

</div>

<div class="row">
			<div class="col-sm-8">
			
			<h4 class="font800">   Real Time, Validated & measurable Information  </h4>
		
		</div>
			<div class="col-sm-4">
								<ul class="flickr-list">
									<li><img alt="" src="images/f2.png"></li>
									
								</ul>
	</div>

</div>

<div class="row">
			<div class="col-sm-4">
			
								<ul class="flickr-list">
									<li><img alt="" src="images/f3.png"></li>
									
								</ul>
		
		</div>
		<div class="col-sm-8">
		<h4 class="font800">GEO Tagged Reports</h4>
	</div>

</div>
<div class="row">
			<div class="col-sm-8 bleft">
			
			<h4 class="font800">   Actionable & Insightful Reports </h4>
		
		</div>
			<div class="col-sm-4">
								<ul class="flickr-list">
									<li><img alt="" src="images/f4.png"></li>
									
								</ul>
	</div>

</div>



</div>
			 
			
			


			

			

			

			

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
			<?php include'footer.php';
?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
	<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:500,

					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off


					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


					fullWidth:"on",

					shadow:1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});


				// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
				// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
					api.bind("revolution.slide.onloaded",function (e) {


						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);

							var timer = setInterval(function() {

								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})

						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
				// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
			});
	</script>
	<script>
		jQuery(function(){ 
			DevSolutionSkill.init('circle'); 
			DevSolutionSkill.init('circle2'); 
			DevSolutionSkill.init('circle3'); 
			DevSolutionSkill.init('circle4'); 
			DevSolutionSkill.init('circle5'); 
			DevSolutionSkill.init('circle6');
		});
	</script>
</body>

</html>