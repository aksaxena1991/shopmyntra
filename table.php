<html>
    <head>
        <title></title>
        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css">
  
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
  
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script>

<!--        <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>-->
        <script>
            $(document).ready(function() {
    $('#data').dataTable();
} );
        </script>
    </head>
    <body>
        <?php

$connection = mysql_connect('localhost' , 'root', '');
$database_name = "audit";
if($connection)
{
    mysql_select_db($database_name);
}
else
{
    die(mysql_error());
}
$sql = "select * from register";
$execute = mysql_query($sql);
?>
        <a href="export.php">Export into Excel File</a>
        <table id="data" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
        <tr>
            <th>
                Register Id
            </th>
            <th>
                First Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                Gender
            </th>
            <th>
                Residence Address
            </th>
            <th>
                District
            </th>
            <th>
                City
            </th>
            <th>
                State
            </th>
            <th>
                Country
            </th>
            <th>
                Pin Code
            </th>
            <th>
                Indian Citizen
            </th>
            <th>
                Company Name
            </th>
            <th>
                Mobile Number 1
            </th>
            <th>
                Mobile Number 2
            </th>
            <th>
                Land Line Number
            </th>
            <th>
                Email
            </th>
            <th>
                Date Of Birth
            </th>
            <th>
                Annual Income
            </th>
            <th>
                Education
            </th>
            <th>
                Occupation
            </th>
            <th>
                Marital Status
            </th>
            <th>
                Presently Working
            </th>
            <th>
                Additional Comment
            </th>
            <th>
                Smart Phone
            </th>
            <th>
                Camera
            </th>
            <th>
                Laptop
            </th>
            <th>
                Printer
            </th>
            <th>
                Scanner
            </th>
            <th>
                Have you ever worked as mystery shopper before :
            </th>
            <th>
                If yes, for which company did you work for? :
            </th>
            <th>
                Have you even been in contact with the police or justice department? :
            </th>
            <th>
                Curriculum Vitae
            </th>
            <th>
                Bank Account Number
            </th>
            <th>
                Account Holder Name
            </th>
            <th>
                Bank Name
            </th>
            <th>
                IFSC Code
            </th>
            <th>
                Bank Branch Address
            </th>
        </tr>
    </thead>
    <tbody>
        <?php while ($row = mysql_fetch_array($execute))
        {?>
        <tr>
            <td>
               <?php echo $row['id'];?> 
            </td>
            <td>
               <?php echo $row['fname'];?> 
            </td>
            <td>
               <?php echo $row['lname'];?> 
            </td>
            <td>
               <?php echo $row['gender'];?> 
            </td>
            <td>
               <?php echo $row['residence'];?> 
            </td>
            <td>
               <?php echo $row['distt'];?> 
            </td>
            <td>
               <?php echo $row['city'];?> 
            </td>
            <td>
               <?php echo $row['state'];?> 
            </td>
            <td>
               <?php echo $row['country'];?> 
            </td>
            <td>
               <?php echo $row['pin'];?> 
            </td>
            <td>
               <?php echo $row['citizen'];?> 
            </td>
            <td>
               <?php echo $row['cname'];?> 
            </td>
            <td>
               <?php echo $row['mob1'];?> 
            </td>
            <td>
               <?php echo $row['mob2'];?> 
            </td>
            <td>
               <?php echo $row['land'];?> 
            </td>
            <td>
               <?php echo $row['email'];?> 
            </td>
            <td>
               <?php echo $row['dob'];?> 
            </td>
            <td>
               <?php echo $row['house_income'];?> 
            </td>
            <td>
               <?php echo $row['edu'];?> 
            </td>
            <td>
               <?php echo $row['occu'];?> 
            </td>
            <td>
               <?php echo $row['marital'];?> 
            </td>
            <td>
               <?php echo $row['p_working'];?> 
            </td>
            <td>
               <?php echo $row['add_comment'];?> 
            </td>
            <td>
               <?php echo $row['smart'];?> 
            </td>
            <td>
               <?php echo $row['cam'];?> 
            </td>
            <td>
               <?php echo $row['laptop'];?> 
            </td>
            <td>
               <?php echo $row['printer'];?> 
            </td>
            <td>
               <?php echo $row['scanner'];?> 
            </td>
            <td>
               <?php echo $row['q1'];?> 
            </td>
            <td>
               <?php echo $row['a1'];?> 
            </td>
            <td>
               <?php echo $row['q2'];?> 
            </td>
            <td>
                <a href="../cv/<?php echo $row['cv'];?>">Download CV</a> 
            </td>
            <td>
                <?php echo $row['bank_detail'];?>
            </td>
            <td>
                <?php echo $row['acc_name'];?>
            </td>
            <td>
                <?php echo $row['bank_name'];?>
            </td>
            <td>
                <?php echo $row['ifsc'];?>
            </td>
            <td>
                <?php echo $row['bank_add'];?>
            </td>
        </tr>
        <?php }?>
    </tbody>
</table>
    </body>
</html>