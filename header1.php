<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img alt="" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="drop"><a class="active" href="index.html">Home</a>
								
							</li>
							
							<li><a href="about.html">About Us</a></li>
							<li class="drop"><a href="blog-right-sidebar.html">Blog</a>
								<ul class="drop-down">
									<li><a href="blog-one-col.html">Blog 1col</a></li>
									<li><a href="blog-two-col.html">Blog 2col</a></li>
									<li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
									<li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
									<li><a href="blog-nosidebar.html">Blog no sidebar</a></li>
								</ul>
							</li>
							<li class="drop"><a href="portfolio-3col.html">Portfolio</a>
								<ul class="drop-down">
									<li><a href="portfolio-2col.html">Portfolio 2col</a></li>
									<li><a href="portfolio-3col.html">Portfolio 3col</a></li>
									<li><a href="portfolio-4col.html">Portfolio 4col</a></li>
								</ul>
							</li>
							<li><a href="shop.html">Shop</a></li>
							<li class="drop"><a href="#">Pages</a>
								<ul class="drop-down">
									<li><a href="single-post.html">Single Post</a></li>
									<li><a href="single-project.html">Single Project</a></li>
								</ul>
							</li>
							<li><a href="contact.html">Contact Us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>