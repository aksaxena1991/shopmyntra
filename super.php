<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>SuperMart</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Industries</a></li>
						<li><a href="#">Super Mart</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="single-project-content">
								<img alt="" src="upload/single-project.jpg">
								<h2>Super Mart</h2>
								<p>For retailers, the seemingly smallest aspects of the in-store experience can mean millions in annual sales across many locations. Product availability, employee up-selling, verbal promotion reminders, promotional signage and more, all hold insurmountable revenue potential. From high-end specialty brands and big box value chains to department stores, convenience stores and gas stations, it can be particularly challenging for retailers today to juggle, drive and prioritize this endless list of performance areas.</p>
								<p>Thats why Market Force provides not only the data you need to monitor these areas but also the analytics and technology platform you need to prioritize and improve on them to predictably drive same-store sales across your organization. </p>
							<p><b>The Science of the Sale</b></p>
							<p>o	Assess operational performance and brand compliance in areas such as sales person knowledge, cleanliness, up-selling and check out with mystery shopping.<br/>
o	Monitor in-store realities such as product availability, pricing accuracy, promotional materials display and how your pricing compares to your competitors with retail audits.<br/>
o	Understand customer attitudes and perceptions on topics such as store layout, product selection, sales person approach, and customer service with customer satisfaction solutions.<br/>
o	Use Market Force to help you execute promotions and seasonal floor sets correctly and on time with merchandising services.<br/>
o	Understand what operational compliance areas matter the most to your sales. Identify what your customers want from their experience based on our unique segmentation analytics. Grow your business using insights like these with ground-breaking advanced analytics and predictive statistical models.<br/>
o	Put all of this into action with one completely integrated customer intelligence technology platform, KnowledgeForceSM, that pushes results down to the store level for maximum accountability. <br/>
</p>
							
							
							
							</div>
						</div>

						
					</div>

					<div class="latest-projects">
						<h3>Latest Projects</h3>
						<div class="row">
							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image1.jpg">
										<div class="hover-box">
											<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Attractive Admin UI</h2>
										<span>short description of portfolio item</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image2.jpg">
										<div class="hover-box">
											<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Green Leaf Falling Dawn</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image3.jpg">
										<div class="hover-box">
											<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Beautiful Wallpaper</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

						</div>
					</div>
											
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
	<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>