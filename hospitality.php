<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Restaurant/Hospitality</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Industries</a></li>
						<li><a href="#">Restaurant/Hospitality</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="single-project-content">
								<img alt="" src="upload/single-project.jpg">
								<h2>Restaurant/Hospitality</h2>
								<b>From Return to Recommendation: What is your plan to win?</b>
								<p>Fast service. Friendly staff. Family Friendly. Healthy food options. Or especially in todays economy: offers the best value for the price. Whether you are a quick serve, fast casual or fine dining restaurant, you are busy perfecting your core competency while staying on top of competitive trends.  So what is your strategy for delighting customers? </p>
								<p>We work with some of the biggest and most successful restaurants in the world.  With the industrys best customer satisfaction surveys, mystery shopping and Quick Serve Restaurant Price Tracking solutions, Market Force delivers the information, analytics andaction tools proven to drive maximum loyalty, frequency of visit, customer recommendations and average ticket size.
 
</p><br/>
<h4>Make the Most of Every Guest</h4><br/>
									<p>o	Assess operational performance in critical areas such as staff attentiveness, speed of service and order accuracy, up-selling and cleanliness with Operational Excellence solutions such as Mystery Shopping, retail audits and more<br/>
o	Understand guest attitudes, perceptions and likelihood to recommend on topics including menu selection, food quality, ambiance and service with Customer Satisfaction information<br/>
o	Understand what your customers want from their experience based on our unique segmentation analysis. Calculate which operational changes will have the most impact based on your financial data and where your missed revenue opportunities lie. Learn what aspects of your customer's experience influence other aspects in a guests mind. Growing your business using insights like these with ground-breaking Advanced Analytics such as the Loyalty Lift Calculator.<br/>
o	Put it all of this into action with KnowledgeForceSM, a completely integrated customer intelligence technology platform that pushes results down to the store level for maximum accountability.<br/>
</p>
								
								

							</div>
						</div>

						
					</div>

					<div class="latest-projects">
						<h3>Latest Projects</h3>
						<div class="row">
							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image1.jpg">
										<div class="hover-box">
											<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Attractive Admin UI</h2>
										<span>short description of portfolio item</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image2.jpg">
										<div class="hover-box">
											<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Green Leaf Falling Dawn</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image3.jpg">
										<div class="hover-box">
											<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Beautiful Wallpaper</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

						</div>
					</div>
											
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
	<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>