<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Mystery Shopping</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Mystery Shopping</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="blog-box with-sidebar">
				<div class="container">
					<div class="row">

						<div class="col-md-9 single-post">
							<div class="single-post-content">
								<img alt="" src="upload/mystery.jpg">
								<h2>Mystery Shopping</h2><hr>
								<p>Mystery shopping in a retail, hotel, restaurant or automotive environment helps you identify gaps in expectation and take the necessary remedial action. It will raise awareness of your procedures and improve staff conduct. As a result you will see customer service levels improve which in turn lead to increased sales.
</p>
								<p>Customer satisfaction information can show you where to invest to maximize customer loyalty, how to turn everyday customers into delighted customers, and even help you recover unsatisfied customers before they write off your brand forever.</p>
						<p>	At AUDIT DADDY we work with you to understand your organization, its procedures and code of conduct. Posing as a real customer our mystery guest will perform the role and activities of any regular guest or customer. Our mystery shopping services are well planned and executed </p>

			<!--					 <ul class="blog-tags">
									<li><a class="autor" href="#"><i class="fa fa-user"></i> TrendyStuff</a></li>
									<li><a class="date" href="#"><i class="fa fa-clock-o"></i> 19 October, 2013</a></li>
									<li><a class="comment-numb" href="#"><i class="fa fa-comments"></i> 16 Comments</a></li>
								</ul> 
			-->				</div>

							


				




							

							
						</div>

						<div class="col-md-3 sidebar">
							<h3>Send us a message</h3>
							<form id="contact-form" method = "POST" action="sendUs.php">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

								
									
									<div class="float-input">
									<input name="phone" id="phone" type="text" placeholder="Phone No">
										<span><i class="fa fa-phone"></i></span>
									</div>
									
										
								</div>
						<div class="float-input3">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span></span>
									</div>
								<div class="textarea-input">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div id="msg" class="message"></div>
								<input type="submit" id="submit_contact" value="Send Message">

							</form>
						</div>
					</div>
					
					
						
				
				
				
				
						


						
				</div>
				
				
			
				
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>