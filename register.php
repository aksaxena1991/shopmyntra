<?php include 'header.php';
?>
<!-- End Header -->

<!-- content 
			================================================== -->
<div id="content">

    <!-- Page Banner -->
    <div class="page-banner">
        <div class="container">
            <h2>Personal Details</h2>
            <ul class="page-tree">
                <li><a href="#">Home</a>
                </li>
                <li><a href="#">Personal Details</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="slider">
        <!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner">
                <ul>
                    <!-- THE FIRST SLIDE -->

                    <!-- THE third SLIDE -->
                    <li data-transition="turnoff" data-slotamount="10" data-masterspeed="300">
                        <!-- THE MAIN IMAGE IN THE second SLIDE -->
                        <img alt="" src="upload/slider-revolution/3.jpg">

                        <!-- THE CAPTIONS IN THIS SLDIE -->

                        <div class="caption randomrotate" data-x="100" data-y="122" data-speed="600" data-start="1200" data-easing="easeOutExpo" data-end="700000" data-endspeed="300" data-endeasing="easeInSine">
                            <img src="images/searching-for-a-scholarship.png" alt="Image 1">
                        </div>

                        <div class="caption big_white sft stt slide3-style" data-x="520" data-y="194" data-speed="500" data-start="1500" data-easing="easeOutExpo" data-end="7300000" data-endspeed="300" data-endeasing="easeInSine">Be a part of<span>AUDIT DADDY </span> 
                        </div>

                        <div class="caption modern_medium_fat sft stt slide3-style" data-x="520" data-y="237" data-speed="500" data-start="1700" data-easing="easeOutExpo" data-end="7500000" data-endspeed="300" data-endeasing="easeInSine"><span class="opt-style2">Become an </span> Auditor</div>

                        <div class="caption lfr medium_text" data-x="520" data-y="278" data-speed="600" data-start="1900" data-easing="easeOutExpo" data-end="7600000" data-endspeed="300" data-endeasing="easeInSine"><a href="register.php">Register</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End slider -->

    <!-- Map box -->
    <!--	<div class="map">

			</div>

			<!-- contact box -->
    <div class="contact-box" style="margin-top:100px;">

        <div class="row">
            <form method="post" id="form1" action="mail.php" enctype="multipart/form-data">
                <div class="bs-example" data-example-id="simple-carousel">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="row padding10" id="tab1">

                                    <div class="col-md-4">
                                        <div class="contact-information">
                                            <img class="img-responsive img-circle wid300" id="blah" src="images/avatar3.png" />
                                        </div>
                                        <span>
                                            <button type="button" class="btn btn-primary mar65" id="upload">Upload Profile Picture</button>
                                            <input type='file' onchange="readURL(this);" style="visibility:hidden;" id="user_img" name="user_img"/>
                                            
                                        </span>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">First Name:</label>
                                            <span class="red">*</span><input type="text" class="form-control" id="fname" name="fname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Last Name:</label>
                                             <span class="red">*</span><input type="text" class="form-control" id="lname" name="lname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Company Name:</label>
                                            <input type="text" class="form-control" id="cname" name="cname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Date Of Birth:</label>
                                             <span class="red">*</span><input type="text" class="form-control" name="dob" id="dob">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Gender:</label>
                                             <span class="red">*&nbsp;</span><label class="radio-inline">
                                                <input type="radio" id="gender" name="gender">Male</label>
                                            <label class="radio-inline">
                                                <input type="radio" id="gender" name="gender">Female</label>

                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Res. Address</label>
                                             <span class="red">*</span><input type="text" class="form-control" name="residence" id="add">
                                        </div>
                                        <div class="form-group">
                                             <label for="pwd">Pin Code :</label>
                                            <span class="red">*</span><input type="text" class="form-control" name="pin">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">City :</label>
                                             <span class="red">*</span><input type="text" class="form-control" name="city">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Distt :</label>
                                            <input type="text" class="form-control" name="distt" id="distt">
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">State :</label>
                                            <!-- <input type="text" class="form-control" name="state"> -->
											 <span class="red">*</span><select class="form-control" name="states">
<option value=''> -- Select -- </option>
<option value='Andaman and Nicobar Islands'>Andaman and Nicobar Islands</option>
<option value='Andhra Pradesh'>Andhra Pradesh</option>
<option value='Arunachal Pradesh'>Arunachal Pradesh</option>
<option value='Assam'>Assam</option>
<option value='Bihar'>Bihar</option>
<option value='Chandigarh'>Chandigarh</option>
<option value='Chhattisgarh'>Chhattisgarh</option>
<option value='Dadra and Nagar Haveli'>Dadra and Nagar Haveli</option>
<option value='Daman and Diu'>Daman and Diu</option>
<option value='Delhi'>Delhi</option>
<option value='Goa'>Goa</option>
<option value='Gujarat'>Gujarat</option>
<option value='Haryana'>Haryana</option>
<option value='Himachal Pradesh'>Himachal Pradesh</option>
<option value='Jammu and Kashmir'>Jammu and Kashmir</option>
<option value='Jharkhand'>Jharkhand</option>
<option value='Karnataka'>Karnataka</option>
<option value='Kerala'>Kerala</option>
<option value='Lakshadweep'>Lakshadweep</option>
<option value='Madhya Pradesh'>Madhya Pradesh</option>
<option value='Maharashtra'>Maharashtra</option>
<option value='Manipur'>Manipur</option>
<option value='Meghalaya'>Meghalaya</option>
<option value='Mizoram'>Mizoram</option>
<option value='Nagaland'>Nagaland</option>
<option value='Odisha'>Odisha</option>
<option value='Puducherry'>Puducherry</option>
<option value='Punjab'>Punjab</option>
<option value='Rajasthan'>Rajasthan</option>
<option value='Sikkim'>Sikkim</option>
<option value='Tamil Nadu'>Tamil Nadu</option>
<option value='Telengana'>Telengana</option>
<option value='Tripura'>Tripura</option>
<option value='Uttar Pradesh'>Uttar Pradesh</option>
<option value='Uttarakhand'>Uttarakhand</option>
<option value='West Bengal'>West Bengal</option>
</select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Country :</label>
											 <span class="red">*</span><select class="form-control" name="states">
<option value=''>Select</option>
<option value='India'>India</option>
<option value='Afghanistan'>Afghanistan</option>
<option value='Aland Islands'>Aland Islands</option>
<option value='Albania'>Albania</option>
<option value='Algeria'>Algeria</option>
<option value='American Samoa'>American Samoa</option>
<option value='Andorra'>Andorra</option>
<option value='Angola'>Angola</option>
<option value='Anguilla'>Anguilla</option>
<option value='Antarctica'>Antarctica</option>
<option value='Antigua and Barbuda'>Antigua and Barbuda</option>
<option value='Argentina'>Argentina</option>
<option value='Armenia'>Armenia</option>
<option value='Aruba'>Aruba</option>
<option value='Australia'>Australia</option>
<option value='Austria'>Austria</option>
<option value='Azerbaijan'>Azerbaijan</option>
<option value='Bahrain'>Bahrain</option>
<option value='Bangladesh'>Bangladesh</option>
<option value='Barbados'>Barbados</option>
<option value='Belarus'>Belarus</option>
<option value='Belgium'>Belgium</option>
<option value='Belize'>Belize</option>
<option value='Benin'>Benin</option>
<option value='Bermuda'>Bermuda</option>
<option value='Bhutan'>Bhutan</option>
<option value='Bolivia'>Bolivia</option>
<option value='Bosnia and Herzegovina'>Bosnia and Herzegovina</option>
<option value='Botswana'>Botswana</option>
<option value='Bouvet Island'>Bouvet Island</option>
<option value='Brazil'>Brazil</option>
<option value='British Indian Ocean Territory'>British Indian Ocean Territory</option>
<option value='British Virgin Islands'>British Virgin Islands</option>
<option value='Brunei'>Brunei</option>
<option value='Bulgaria'>Bulgaria</option>
<option value='Burkina Faso'>Burkina Faso</option>
<option value='Burundi'>Burundi</option>
<option value='Cambodia'>Cambodia</option>
<option value='Cameroon'>Cameroon</option>
<option value='Canada'>Canada</option>
<option value='Cape Verde'>Cape Verde</option>
<option value='Caribbean Netherlands'>Caribbean Netherlands</option>
<option value='Cayman Islands'>Cayman Islands</option>
<option value='Central African Republic'>Central African Republic</option>
<option value='Chile'>Chile</option>
<option value='China'>China</option>
<option value='Christmas Island'>Christmas Island</option>
<option value='Cocos (Keeling) Islands'>Cocos (Keeling) Islands</option>
<option value='Colombia'>Colombia</option>
<option value='Comoros'>Comoros</option>
<option value='Congo'>Congo</option>
<option value='Cook Islands'>Cook Islands</option>
<option value='Costa Rica'>Costa Rica</option>
<option value='Croatia'>Croatia</option>
<option value='Cuba'>Cuba</option>
<option value='Curacao'>Curacao</option>
<option value='Cyprus'>Cyprus</option>
<option value='Czech Republic'>Czech Republic</option>
<option value='Democratic Republic of Congo'>Democratic Republic of Congo</option>
<option value='Denmark'>Denmark</option>
<option value='Disputed Territory'>Disputed Territory</option>
<option value='Djibouti'>Djibouti</option>
<option value='Dominica'>Dominica</option>
<option value='Dominican Republic'>Dominican Republic</option>
<option value='East Timor'>East Timor</option>
<option value='Ecuador'>Ecuador</option>
<option value='Egypt'>Egypt</option>
<option value='El Salvador'>El Salvador</option>
<option value='Equatorial Guinea'>Equatorial Guinea</option>
<option value='Eritrea'>Eritrea</option>
<option value='Estonia'>Estonia</option>
<option value='Ethiopia'>Ethiopia</option>
<option value='Falkland Islands'>Falkland Islands</option>
<option value='Faroe Islands'>Faroe Islands</option>
<option value='Federated States of Micronesia'>Federated States of Micronesia</option>
<option value='Fiji'>Fiji</option>
<option value='Finland'>Finland</option>
<option value='France'>France</option>
<option value='French Guiana'>French Guiana</option>
<option value='French Polynesia'>French Polynesia</option>
<option value='Gabon'>Gabon</option>
<option value='Georgia'>Georgia</option>
<option value='Germany'>Germany</option>
<option value='Ghana'>Ghana</option>
<option value='Gibraltar'>Gibraltar</option>
<option value='Greece'>Greece</option>
<option value='Greenland'>Greenland</option>
<option value='Grenada'>Grenada</option>
<option value='Guadeloupe'>Guadeloupe</option>
<option value='Guam'>Guam</option>
<option value='Guatemala'>Guatemala</option>
<option value='Guinea'>Guinea</option>
<option value='Guinea-Bissau'>Guinea-Bissau</option>
<option value='Guyana'>Guyana</option>
<option value='Haiti'>Haiti</option>
<option value='Heard Island and McDonald Islands'>Heard Island and McDonald Islands</option>
<option value='Honduras'>Honduras</option>
<option value='Hong Kong'>Hong Kong</option>
<option value='Hungary'>Hungary</option>
<option value='Iceland'>Iceland</option>

<option value='Indonesia'>Indonesia</option>
<option value='Iran'>Iran</option>
<option value='Iraq'>Iraq</option>
<option value='Iraq-Saudi Arabia Neutral Zone'>Iraq-Saudi Arabia Neutral Zone</option>
<option value='Ireland'>Ireland</option>
<option value='Israel'>Israel</option>
<option value='Italy'>Italy</option>
<option value='Ivory Coast'>Ivory Coast</option>
<option value='Jamaica'>Jamaica</option>
<option value='Japan'>Japan</option>
<option value='Jordan'>Jordan</option>
<option value='Kazakhstan'>Kazakhstan</option>
<option value='Kenya'>Kenya</option>
<option value='Kiribati'>Kiribati</option>
<option value='Kuwait'>Kuwait</option>
<option value='Kyrgyzstan'>Kyrgyzstan</option>
<option value='Laos'>Laos</option>
<option value='Latvia'>Latvia</option>
<option value='Lebanon'>Lebanon</option>
<option value='Lesotho'>Lesotho</option>
<option value='Liberia'>Liberia</option>
<option value='Libya'>Libya</option>
<option value='Liechtenstein'>Liechtenstein</option>
<option value='Lithuania'>Lithuania</option>
<option value='Luxembourg'>Luxembourg</option>
<option value='Macau'>Macau</option>
<option value='Macedonia'>Macedonia</option>
<option value='Madagascar'>Madagascar</option>
<option value='Malawi'>Malawi</option>
<option value='Malaysia'>Malaysia</option>
<option value='Maldives'>Maldives</option>
<option value='Mali'>Mali</option>
<option value='Malta'>Malta</option>
<option value='Marshall Islands'>Marshall Islands</option>
<option value='Martinique'>Martinique</option>
<option value='Mauritania'>Mauritania</option>
<option value='Mauritius'>Mauritius</option>
<option value='Mayotte'>Mayotte</option>
<option value='Mexico'>Mexico</option>
<option value='Moldova'>Moldova</option>
<option value='Monaco'>Monaco</option>
<option value='Mongolia'>Mongolia</option>
<option value='Montenegro'>Montenegro</option>
<option value='Montserrat'>Montserrat</option>
<option value='Morocco'>Morocco</option>
<option value='Mozambique'>Mozambique</option>
<option value='Myanmar'>Myanmar</option>
<option value='Namibia'>Namibia</option>
<option value='Nauru'>Nauru</option>
<option value='Nepal'>Nepal</option>
<option value='Netherlands'>Netherlands</option>
<option value='New Caledonia'>New Caledonia</option>
<option value='New Zealand'>New Zealand</option>
<option value='Nicaragua'>Nicaragua</option>
<option value='Niger'>Niger</option>
<option value='Nigeria'>Nigeria</option>
<option value='Niue'>Niue</option>
<option value='Norfolk Island'>Norfolk Island</option>
<option value='North Korea'>North Korea</option>
<option value='Northern Mariana Islands'>Northern Mariana Islands</option>
<option value='Norway'>Norway</option>
<option value='Oman'>Oman</option>
<option value='Pakistan'>Pakistan</option>
<option value='Palau'>Palau</option>
<option value='Palestine'>Palestine</option>
<option value='Panama'>Panama</option>
<option value='Papua New Guinea'>Papua New Guinea</option>
<option value='Paraguay'>Paraguay</option>
<option value='Peru'>Peru</option>
<option value='Philippines'>Philippines</option>
<option value='Pitcairn Islands'>Pitcairn Islands</option>
<option value='Poland'>Poland</option>
<option value='Portugal'>Portugal</option>
<option value='Puerto Rico'>Puerto Rico</option>
<option value='Qatar'>Qatar</option>
<option value='Reunion'>Reunion</option>
<option value='Romania'>Romania</option>
<option value='Russia'>Russia</option>
<option value='Rwanda'>Rwanda</option>
<option value='Saint Barthelemy'>Saint Barthelemy</option>
<option value='Saint Kitts and Nevis'>Saint Kitts and Nevis</option>
<option value='Saint Vincent and the Grenadines'>Saint Vincent and the Grenadines</option>
<option value='Saint-Martin'>Saint-Martin</option>
<option value='Saint-Pierre-Et-Miquelon'>Saint-Pierre-Et-Miquelon</option>
<option value='Samoa'>Samoa</option>
<option value='San Marino'>San Marino</option>
<option value='Sao Tome E Principe'>Sao Tome E Principe</option>
<option value='Saudi Arabia'>Saudi Arabia</option>
<option value='Senegal'>Senegal</option>
<option value='Serbia'>Serbia</option>
<option value='Seychelles'>Seychelles</option>
<option value='Sierra Leone'>Sierra Leone</option>
<option value='Singapore'>Singapore</option>
<option value='Sint Maarten'>Sint Maarten</option>
<option value='Slovakia'>Slovakia</option>
<option value='Slovenia'>Slovenia</option>
<option value='Solomon Islands'>Solomon Islands</option>
<option value='Somalia'>Somalia</option>
<option value='South Africa'>South Africa</option>
<option value='South Georgia and the South Sandwich Islands'>South Georgia and the South Sandwich Islands</option>
<option value='South Korea'>South Korea</option>
<option value='Spain'>Spain</option>
<option value='Spratly Islands'>Spratly Islands</option>
<option value='Sri Lanka'>Sri Lanka</option>
<option value='St Helena Ascension and Tristan da Cunha'>St Helena Ascension and Tristan da Cunha</option>
<option value='St. Lucia'>St. Lucia</option>
<option value='Sudan'>Sudan</option>
<option value='Suriname'>Suriname</option>
<option value='Svalbard and Jan Mayen'>Svalbard and Jan Mayen</option>
<option value='Swaziland'>Swaziland</option>
<option value='Sweden'>Sweden</option>
<option value='Switzerland'>Switzerland</option>
<option value='Syria'>Syria</option>
<option value='Taiwan'>Taiwan</option>
<option value='Tajikistan'>Tajikistan</option>
<option value='Tanzania'>Tanzania</option>
<option value='Tchad'>Tchad</option>
<option value='Terres Australes Et Antarctiques Francaises'>Terres Australes Et Antarctiques Francaises</option>
<option value='Thailand'>Thailand</option>
<option value='The Bahamas'>The Bahamas</option>
<option value='The Gambia'>The Gambia</option>
<option value='The Republic of South Sudan'>The Republic of South Sudan</option>
<option value='Togo'>Togo</option>
<option value='Tokelau'>Tokelau</option>
<option value='Tonga'>Tonga</option>
<option value='Trinidad and Tobago'>Trinidad and Tobago</option>
<option value='Tunisia'>Tunisia</option>
<option value='Turkey'>Turkey</option>
<option value='Turkmenistan'>Turkmenistan</option>
<option value='Turks and Caicos Islands'>Turks and Caicos Islands</option>
<option value='Tuvalu'>Tuvalu</option>
<option value='US Virgin Islands'>US Virgin Islands</option>
<option value='Uganda'>Uganda</option>
<option value='Ukraine'>Ukraine</option>
<option value='United Arab Emirates'>United Arab Emirates</option>
<option value='United Kingdom'>United Kingdom</option>
<option value='United Nations Neutral Zone'>United Nations Neutral Zone</option>
<option value='United States'>United States</option>
<option value='United States Minor Outlying Islands'>United States Minor Outlying Islands</option>
<option value='Uruguay'>Uruguay</option>
<option value='Uzbekistan'>Uzbekistan</option>
<option value='Vanuatu'>Vanuatu</option>
<option value='Vatican City'>Vatican City</option>
<option value='Venezuela'>Venezuela</option>
<option value='Vietnam'>Vietnam</option>
<option value='Wallis-Et-Futuna'>Wallis-Et-Futuna</option>
<option value='Western Sahara'>Western Sahara</option>
<option value='Yemen'>Yemen</option>
<option value='Zambia'>Zambia</option>
<option value='Zimbabwe'>Zimbabwe</option>
</select>
                                           <!--  <input type="text" class="form-control" name="country">    -->
										   </div>
                                        <div class="form-group">
                                            <label for="sel1">Indian Citizen:</label>
                                            <select class="form-control" name="citizen" id="citizen">
												<option value="">Select</option>
                                                <option>YES</option>
                                                <option>NO</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Mobile No. 1</label>
                                            <span class="red">*</span> <input type="text" class="form-control" name="mob1">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Mobile No. 2</label>
                                            <input type="text" class="form-control" name="mob2" id="m2">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Landline No.</label>
                                            <input type="text" class="form-control" name="land" id="land">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Email ID</label>
                                             <span class="red">*</span><input type="email" class="form-control" name="email" id="email1" >
                                        </div>

                                        <div id="msg" class="message"></div>
                                        <div class="top1">
                                            <a class="right carousel-control" id="next1" href="#carousel-example-generic" role="button" data-slide="next" style="right:16px;top:580px;">
                                                Next
												 
                                                 
                                            </a>
                                        </div>
	

                                    </div>
									 

                                </div>
								  
									<div class="visible-xs">
                                            <a class="right carousel-control top4" id="next11" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												 
                                            </a>
                                        </div>
								 
                            </div>
                            <div class="item">
                                <div class="row padding10" id="tab2">
                                    <h2 class="pad14">Location Details</h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Household Income:</label>
                                            <span class="red">*</span> <input type="text" class="form-control" name="house_income">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Education:</label>
                                            <!--<input type="text" class="form-control" name="edu">-->
											 <span class="red">*</span><select class="form-control" name="edu">
                                                <option value="">Select</option>
												<option>SSC</option>
                                                <option>HSC</option>
												<option>Graduation</option>
												<option>Post Graduation</option>
												<option>Other</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Marital Status:</label>
                                            <!-- <input type="text" class="form-control" name="marital"> -->
											 <span class="red">*</span><select class="form-control" name="marital">
                                                <option value="">Select</option>
												<option>Married</option>
                                                <option>Unmarried</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Presently working:</label>
                                            <select id="pr" class="form-control" name="p_working">
                                                <option value="">Select</option>
												<option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Occupation:</label>
                                            <input type="text" class="form-control" name="occu" id="occu">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Additional Comments:</label>
                                            <input id="com" type="text" class="form-control" name="add_comment">
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" id="next2" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="item">
                                <div class="row padding10">

                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">SmartPhone:</label>
                                            <select class="form-control" name="smart">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Camera Resolution:</label>
                                            <select class="form-control" name="cam">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Laptop:</label>
                                            <select class="form-control" name="laptop">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Access to Scanner:</label>
                                            <select class="form-control" name="scanner">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sel1">Access to Printer:</label>
                                            <select class="form-control" name="printer">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="visible-xs">
										
                                            <a class="left carousel-control top5" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control top6" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
										
                                </div>

                                </div>
								
                            </div>

                            <div class="item">
                                <div class="row padding10">
                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Have you ever worked as mystery shopper before :</label>
                                            <select class="form-control" name="q1">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="sel1">If yes, for which company did you work for? :</label>
                                            <select class="form-control" name="a1">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Have you even been in contact with the police or justice department? :</label>
                                            <select class="form-control" name="q2">
                                                <option>NO</option>
                                                <option>YES</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Please upload your CV to complete your registration:</label>
                                            <input type="file" name="cv" id="cv" class="form-control" />
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="visible-xs">
                                            <a class="left carousel-control top7 top10" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control top8 top11" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>

                                </div>
                            </div>

                            <div class="item">
                                <div class="row padding10" id="tab5">
                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Bank Account Number :</label>
                                            <input type="text" class="form-control" name="bank_detail">

                                            <div class="top3 hidden-xs" >
                                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                    Back
													<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Account holders Name :</label>
                                            <input type="text" class="form-control" name="acc_name">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Bank Name :</label>
                                            <input type="text" class="form-control" name="bank_name">
                                        </div>

                                    </div>

                                    <div class="col-md-4" id="contact-form">

                                        <div class="form-group">
                                            <label for="usr">IFSC Code:</label>
                                            <input type="text" class="form-control" name="ifsc">
                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Bank Branch Address:</label>
                                            <input type="text" class="form-control" name="bank_add">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Send" id="submit" class="btn big_white"/>
                                        </div>

            

            </div>
			 <div class="visible-xs">
                                                <a class="left carousel-control top9" href="#carousel-example-generic" role="button" data-slide="prev">
                                                    Back
													<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>

                                            </div>

            </div>
            </div>

            </div>

            </div>
            </div>
            </form>

        </div>
    </div>

</div>
<!-- End content -->

<!-- footer 
			================================================== -->
<?php include 'footer.php'; ?>
<!-- End footer -->

<div class="fixed-link-top">
    <div class="container">
        <a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
    </div>
</div>

</div>
<!-- End Container -->

<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
<script type="text/javascript">
    var tpj = jQuery;
    tpj.noConflict();

    tpj(document).ready(function() {

        if (tpj.fn.cssOriginal != undefined)
            tpj.fn.css = tpj.fn.cssOriginal;

        var api = tpj('.fullwidthbanner').revolution({
            delay: 100000,
            startwidth: 1170,
            startheight: 500,
            interval: false,
            onHoverStop: "on", // Stop Banner Timet at Hover on Slide on/off

            thumbWidth: 100, // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
            thumbHeight: 50,
            thumbAmount: 3,

            hideThumbs: 0,
            navigationType: "none", // bullet, thumb, none
            navigationArrows: "solo", // nexttobullets, solo (old name verticalcentered), none

            navigationStyle: "round", // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom

            navigationHAlign: "center", // Vertical Align top,center,bottom
            navigationVAlign: "bottom", // Horizontal Align left,center,right
            navigationHOffset: 30,
            navigationVOffset: 40,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 0,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 0,
            soloArrowRightVOffset: 0,

            touchenabled: "on", // Enable Swipe Function : on/off

            stopAtSlide: -1, // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops: -1, // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

            hideCaptionAtLimit: 0, // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit: 0, // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit: 0, // Hide the whole slider, and stop also functions if Width of Browser is less than this value

            fullWidth: "on",

            shadow: 1 //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

        });

        // TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
        // YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
        api.bind("revolution.slide.onloaded", function(e) {

            jQuery('.tparrows').each(function() {
                var arrows = jQuery(this);

                var timer = setInterval(function() {

                    if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
                        arrows.fadeOut(300);
                }, 3000);
            })

            jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
                jQuery('.tp-simpleresponsive').addClass("mouseisover");
                jQuery('body').find('.tparrows').each(function() {
                    jQuery(this).fadeIn(300);
                });
            }, function() {
                if (!jQuery(this).hasClass("tparrows"))
                    jQuery('.tp-simpleresponsive').removeClass("mouseisover");
            })
        });
        // END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
    });
</script>
<script>
    jQuery(function() {
        DevSolutionSkill.init('circle');
        DevSolutionSkill.init('circle2');
        DevSolutionSkill.init('circle3');
        DevSolutionSkill.init('circle4');
        DevSolutionSkill.init('circle5');
        DevSolutionSkill.init('circle6');
    });
	$(document).ready(function(){
			
			$("#next11").click(function(e){
				var i=0;
				$('#tab1 input[type=text],#tab1 select,#tab1 input[type=radio]').each(function(){
					
					if($(this).val()=='' && $(this).attr("id")!='cname' && $(this).attr("id")!='dob' && $(this).attr("id")!='m2' && $(this).attr("id")!='land'){
					$(this).css("border", "thin solid red");
					i++;
					}
					
				});
				if(i==0){return true;} else{ return false; }
			});
	
			$("#next1").click(function(e){
				var i=0;
				$('#tab1 input[type=text],#tab1 select,#tab1 input[type=radio],#tab1 input[type=email]').each(function(){
					
					if($(this).val()=='' && $(this).attr("id")!='cname'  && $(this).attr("id")!='m2' && $(this).attr("id")!='land'  && $(this).attr("id")!='distt' && $(this).attr("id")!='citizen' ){
					$(this).css("border", "thin solid red");
					i++;
					}
					
				});
				if(i==0){return true;} else{ return false; }
			});
			
			$("#next2").click(function(e){
				var i=0;
				$('#tab2 input[type=text],#tab2 select,#tab2 input[type=radio]').each(function(){
					
					if($(this).val()=='' && $(this).attr("id")!='com' && $(this).attr("id")!='pr'  && $(this).attr("id")!='occu' ){
					$(this).css("border", "thin solid red");
					i++;
					}
					
				});
				if(i==0){return true;} else{ return false; }
			});
			
			$("#form1").submit(function(e){
				var i=0;
				$('#tab5 input[type=text],#tab5 select,#tab5 input[type=radio]').each(function(){
					
					if($(this).val()=='' ){
					$(this).css("border", "thin solid red");
					i++;
					}
					
				});
				if(i==0){
			
					//$("#form1").submit();
					return true;
					} 
					else{ return false; }
			});
			
	})
</script>
</body>

</html>

</html>
