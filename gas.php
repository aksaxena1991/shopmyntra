<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Gas Stations</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Industries</a></li>
						<li><a href="#">Gas Stations</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="single-project-content">
								<img alt="" src="upload/single-project.jpg">
								<h2>Convenience Stores and Gas Stations</h2>
								<p>Fast. Convenient.One-Stop Shopping. It�s the brand promise of every convenience store.  How do you out-win the competition with a superior customer experience? And improve your bottom line with operational excellence?</p>
								<p>With numerous store locations in both metropolitan and remote areas, measuring the system-wide customer experience and keeping an eye on operations presents a challenge for most convenience store and petroleum companies. </p>
							<p>Market Force provides the geographic coverage you need with a field network of over 600,000 individuals. Through our range of products and services we provide the customer intelligence with the insights and on-site action you need to effectively manage your customer�s experience and stay ahead of your competition.</p>
							<p><b>Use Market Force On-Site Services to:</b></p>
							<p>o	Monitor program execution and operational compliance across all your locations with On-Site Audits<br/>
o	Assess operational performance in areas including customer greeting and cleanliness of the store, restrooms and fueling area with mystery shopping evaluations<br/>
o	Verify compliance with branding and pump labeling guidelines<br/>
o	Understand customer attitudes and perceptions on topics including safety, cleanliness and overall satisfaction with customer surveys<br/>
o	Capture key item pricing across your locations and those of your competitors with Pricing Reports<br/>
</p>
							
							<p><b>Use Market Force Data & Analysis Services to:</b></p>
							<p>o	Identify business levers where improvements will generate the highest returns<br/>
o	Uncover additional revenue opportunities<br/>
o	Highlight top performing locations across your organization and identify the drivers of that success<br/>
o	Automatically deliver performance-based training or rewards at the individual location�level<br/>
</p>
							
							</div>
						</div>

					
					</div>

					<div class="latest-projects">
						<h3>Latest Projects</h3>
						<div class="row">
							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image1.jpg">
										<div class="hover-box">
											<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Attractive Admin UI</h2>
										<span>short description of portfolio item</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image2.jpg">
										<div class="hover-box">
											<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Green Leaf Falling Dawn</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image3.jpg">
										<div class="hover-box">
											<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Beautiful Wallpaper</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

						</div>
					</div>
											
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
	<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>