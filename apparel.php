<?php include 'header.php';
?>
	
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Apparel</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Industries</a></li>
						<li><a href="#">Apparel</a></li>
					</ul>				
				</div>
			</div>

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="single-project-content">
								<img alt="" src="upload/single-project.jpg">
								<h2>Green Leaf Falling Dawn</h2>
								<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
								<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
							</div>
						</div>

						
					</div>

					<div class="latest-projects">
						<h3>Latest Projects</h3>
						<div class="row">
							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image1.jpg">
										<div class="hover-box">
											<a class="zoom video" href="https://www.youtube.com/watch?v=h_5sQjRrFIs"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Attractive Admin UI</h2>
										<span>short description of portfolio item</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image2.jpg">
										<div class="hover-box">
											<a class="zoom" href="upload/image2.jpg"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Green Leaf Falling Dawn</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

							<div class="col-md-4">
								<div class="work-post">
									<div class="work-post-gal">
										<img alt="" src="upload/image3.jpg">
										<div class="hover-box">
											<a class="zoom video" href="http://www.youtube.com/watch?v=6v2L2UGZJAM"><i class="fa fa-plus-circle"></i></a>
											<a class="page" href="single-project.html"><i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<div class="work-post-content">
										<h2>Beautiful Wallpaper</h2>
										<span>short description of portfolio item</span>
									</div>										
								</div>
							</div>

						</div>
					</div>
											
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
	<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>