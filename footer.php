	<footer>
			<div class="up-footer">
				<div class="container">
					<div class="row">

						<div class="col-md-3">
							<div class="widget footer-widgets twitter-widget">
								<h4>Audit Daddy</h4>
<!--<ul class="tweets">
									<li>
										<p><a class="tweet-acc" href="#">@premiumlayers</a> Thanks for the head up! :) <a href="#">http://support.auditdaddy.com</a> was very helpful</p>
										<span>3 days ago</span>
									</li>
									<li>
										<p><a class="tweet-acc" href="#">@envato</a> <a href="#">http://support.auditdaddy.com</a> </p>
										<span>4 days ago</span>
									</li>
									<li>
										<p><a class="tweet-acc" href="#">@premiumlayers </a> Well that's awesome thing</p>
										<span>5 days ago</span>
									</li>
								</ul> -->
								<ul class="tag-widget-list">
									<li><a class="phone" href="index.php"><span>Home</span></a></li>
								<li><a class="address" href="it.php"><span> Industries</span></a></li>
									<li><a class="phone" href="mystery.php"><span>Services</span></a></li>
									
									
									<li><a class="mail" href="technology.php"><span>Technology</span></a></li>
									<li><a class="address" href="contact.php"><span>Contact Us</span></a></li>
								</ul>
								
								
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget footer-widgets flickr-widget">
								<h4>Services</h4>
								<ul class="flickr-list">
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr1.png">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="mystery.php">Mystery Shopping</a>
											 </div>
										</div>
									</li>
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr2.png" style="width:64px;">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="onsite.php">Onsite Audit</a>
											 </div>
										</div>
									</li>
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr3.png">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="display.php">Display Audit</a>
											 </div>
										</div>
									</li>
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr4.png">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="servicecentre.php">Service Centre</a>
											 </div>
										</div>
									</li>
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr5.png">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="stock.php">Stock Audit</a>
											 </div>
										</div>
									</li>
									<li>
										<div class="screenshot">
											<img alt="" src="images/flickr6.png">
											 <div class="screenshot-caption screenshot-caption_top">
													<a class="a-hover" href="pos.php">POS Staff Audit</a>
											 </div>
										</div>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget footer-widgets tag-widget">
								<h4>Industries</h4>
								<ul class="tag-widget-list">
									<li><a href="it.php">Apparel</a></li>
									<li><a href="it.php">Telecom</a></li>
									<li><a href="it.php"> IT</a></li>
									<li><a href="it.php">Food & Beverages</a></li>
									<li><a href="it.php">Hospitality</a></li>
									<li><a href="it.php">Gas Station</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget footer-widgets info-widget">
								<h4>Contact Us</h4>
								<ul class="contact-list">
									<li><a class="phone" href="tel:9599114985"><i class="fa fa-phone"></i><span>9599114985 </span></a></li>
									<li><a class="phone" href="tel:9599114983"><i class="fa fa-phone"></i><span>9599114983</span></a></li>
									<li><a class="mail" href="mailto:contact@auditdaddy.com?subject=Buisness Enquiry-AuditDaddy"><i class="fa fa-envelope"></i><span>contact@auditdaddy.com</span></a></li>
									<li><a data-toggle="modal" data-target="#myModal22" class="address" href="#"><i class="fa fa-home"></i><span>D-13/5 Okhla Phase II, Delhi</span></a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="footer-line">
				<div class="container">
					<div class="footer-line-in">
						<div class="row">
							<div class="col-md-8">
								<p>&#169; 2015 AuditDaddy,  All Rights Reserved</p>
							</div>
							<div class="col-md-4">
								<ul class="social-icons">
									<li><a class="facebook" href="https://www.facebook.com/pages/Audit-Daddy/441589799329641?sk=timeline"><i class="fa fa-facebook"></i></a></li>
									<li><a class="twitter" href="https://twitter.com/AuditDaddy"><i class="fa fa-twitter"></i></a></li>
						<!--			<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
							-->		<li><a class="google" href="https://plus.google.com/u/0/109529168694117613081/posts"><i class="fa fa-google-plus"></i></a></li>
									<li><a class="linkedin" href="https://www.linkedin.com/profile/guided?trk=registration&o=reg"><i class="fa fa-linkedin"></i></a></li>
						<!--			<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
							-->	</ul>
							</div>
						</div>						
					</div>
				</div>
			</div>

		</footer>
		<div class="modal fade bs-example-modal-lg" id="myModal22" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span><img class="width-img35" src="img/close.png"></span><span class="sr-only"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Contact Us</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="item-logo">
										<p style="font-size: 17px;"><span><i class="fa fa-home"></i> D-13/5 Okhla Delhi Phase II</span></p><br>
<embed src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.2170519258!2d77.273508!3d28.533195000000017!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce159b8656c01%3A0x50aa47343cae5f73!2sD-13%2F5%2C+Okhla+Industrial+Area+II%2C+Pocket+W%2C+Okhla+Phase+II%2C+Okhla+Industrial+Area%2C+New+Delhi%2C+Delhi+110020!5e0!3m2!1sen!2sin!4v1426559358240" width="100%" height="450" frameborder="0" style="border:0"></embed> </div>
                                </div>
                                 
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>