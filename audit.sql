-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2015 at 11:45 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE IF NOT EXISTS `register` (
`id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `cname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `residence` varchar(255) NOT NULL,
  `pin` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `distt` varchar(255) NOT NULL,
  `citizen` varchar(255) NOT NULL,
  `mob1` varchar(255) NOT NULL,
  `mob2` varchar(255) NOT NULL,
  `land` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `house_income` varchar(255) NOT NULL,
  `edu` varchar(255) NOT NULL,
  `marital` varchar(255) NOT NULL,
  `p_working` varchar(255) NOT NULL,
  `occu` varchar(255) NOT NULL,
  `add_comment` varchar(255) NOT NULL,
  `smart` varchar(255) NOT NULL,
  `cam` varchar(255) NOT NULL,
  `laptop` varchar(255) NOT NULL,
  `scanner` varchar(255) NOT NULL,
  `printer` varchar(255) NOT NULL,
  `q1` varchar(255) NOT NULL,
  `a1` varchar(255) NOT NULL,
  `q2` varchar(255) NOT NULL,
  `cv` varchar(255) NOT NULL,
  `bank_detail` varchar(255) NOT NULL,
  `acc_name` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `bank_add` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `fname`, `lname`, `cname`, `gender`, `residence`, `pin`, `state`, `city`, `country`, `distt`, `citizen`, `mob1`, `mob2`, `land`, `email`, `dob`, `house_income`, `edu`, `marital`, `p_working`, `occu`, `add_comment`, `smart`, `cam`, `laptop`, `scanner`, `printer`, `q1`, `a1`, `q2`, `cv`, `bank_detail`, `acc_name`, `bank_name`, `ifsc`, `bank_add`) VALUES
(2, 'Anubhav', 'Saxena', 'Trion Technologies', 'on', 'B-128,129 Utsav Vihar', '110081', 'Delhi', 'New Delhi', 'India', 'Delhi', 'YES', '9911609299', '9557375517', 'none', 'aksaxena1991@gmail.com', '28/11/1991', '1', 'B.Tech', 'Single', 'YES', 'Private Job', 'None', 'YES', '1px', 'YES', 'YES', 'YES', 'YES', 'YES', 'YES', 'thumb_Hobit_2_titulka_15.jpg', '6190001500004817', 'Anubhav Saxena', 'PNB', 'PUNB0619000', 'Clement Town, Dehradun');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register`
--
ALTER TABLE `register`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
