<?php include 'header.php';
?>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contact Us</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">Contact</a></li>
					</ul>				
				</div>
			</div>

			<!-- Map box -->
		<!--	<div class="map">
				
			</div>

			<!-- contact box -->
			<div class="contact-box" style="margin-top:100px;">
				<div class="container">
					<div class="row">

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Contact info</h3>
								<ul class="contact-information-list">
									<li data-toggle="modal" data-target="#myModal22"><span><i class="fa fa-home"></i>D-13/5 Okhla Phase II Delhi</span></li>
									<li><span><i class="fa fa-phone"></i><a href="tel:9599114985">9599114985</a> </span></li>
									<li><span><i class="fa fa-phone"></i> <a href="tel:9599114983"> 9599114983</a></span></li>
									<li><a href="mailto:info@auditdaddy.com?subject=Buisness Enquiry-AuditDaddy"><i class="fa fa-envelope"></i>info@auditdaddy.com</a></li>
									<li>
										<a href="https://www.facebook.com/pages/Audit-Daddy/441589799329641?sk=timeline"><i class="fa fa-facebook"></i></a>
										<a href="https://twitter.com/AuditDaddy"><i class="fa fa-twitter"></i></a>
					<!--					<a href="#"><i class="fa fa-rss"></i></a>
						-->				<a href="https://plus.google.com/u/0/109529168694117613081/posts"><i class="fa fa-google-plus"></i></a>
										<a href="https://www.linkedin.com/profile/guided?trk=registration&o=reg"><i class="fa fa-linkedin"></i></a>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Working hours</h3>
								<p>If you need more about audit or AUDIT DADDY websites contact with us. We will help you to make successful any of your audit works. Feel free to contact with us through mail address. </p>
								<p class="work-time"><span>Mon - Fri</span> : 10 AM to 6 PM</p>
								
							</div>
						</div>

						<div class="col-md-6">
							<h3>Send us a message</h3>
							<form id="contact-form" method = "POST" action="sendUs.php">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

								
									
									<div class="float-input">
									<input name="phone" id="phone" type="text" placeholder="Phone No">
										<span><i class="fa fa-phone"></i></span>
									</div>
									
										
								</div>
						<div class="float-input3">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span></span>
									</div>
								<div class="textarea-input">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>

								<div id="msg" class="message"></div>
								<input type="submit" id="submit_contact" value="Send Message">

							</form>
						</div>

					</div>
				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<div class="modal fade bs-example-modal-lg" id="myModal22" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span><img class="width-img35" src="img/close.png"></span><span class="sr-only"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Contact Us</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="item-logo">
										<p style="font-size: 17px;"><span><i class="fa fa-home"></i> B-13/5 Okhla Delhi-110020</span></p><br>
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1752.5408487382836!2d77.27507899999999!3d28.537264000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce3fd54de43f1%3A0x1619c22d2f973de6!2sB-13%2C+Okhla+Industrial+Area+II%2C+Pocket+B%2C+Okhla+Phase+II%2C+Okhla+Industrial+Area%2C+New+Delhi%2C+Delhi+110020!5e0!3m2!1sen!2sin!4v1426228379636" "&zoom=14"   frameborder="0" style="border:0;width:540px;height:480px;"></iframe>
                                    </div>
                                </div>
                                 
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
	<!-- End Container -->

	
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>