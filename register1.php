<?php include 'header.php';
?>
<!-- End Header -->
<script>
$('#next1').click(function(){
	$('#first').hide();
		$('#second').slideToggle();
		
	
});
</script>
<!-- content 
			================================================== -->
<div id="content">

    <!-- Page Banner -->
    <div class="page-banner">
        <div class="container">
            <h2>Personal Details</h2>
            <ul class="page-tree">
                <li><a href="#">Home</a>
                </li>
                <li><a href="#">Personal Details</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="slider">
        <!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner">
                <ul>
                    <!-- THE FIRST SLIDE -->

                    <!-- THE third SLIDE -->
                    <li data-transition="turnoff" data-slotamount="10" data-masterspeed="300">
                        <!-- THE MAIN IMAGE IN THE second SLIDE -->
                        <img alt="" src="upload/slider-revolution/3.jpg">

                        <!-- THE CAPTIONS IN THIS SLDIE -->

                        <div class="caption randomrotate" data-x="100" data-y="122" data-speed="600" data-start="1200" data-easing="easeOutExpo" data-end="700000" data-endspeed="300" data-endeasing="easeInSine">
                            <img src="images/searching-for-a-scholarship.png" alt="Image 1">
                        </div>

                        <div class="caption big_white sft stt slide3-style" data-x="520" data-y="194" data-speed="500" data-start="1500" data-easing="easeOutExpo" data-end="7300000" data-endspeed="300" data-endeasing="easeInSine">Be a part of<span>AUDIT DADDY </span> 
                        </div>

                        <div class="caption modern_medium_fat sft stt slide3-style" data-x="520" data-y="237" data-speed="500" data-start="1700" data-easing="easeOutExpo" data-end="7500000" data-endspeed="300" data-endeasing="easeInSine"><span class="opt-style2">Become an </span> Auditor</div>

                        <div class="caption lfr medium_text" data-x="520" data-y="278" data-speed="600" data-start="1900" data-easing="easeOutExpo" data-end="7600000" data-endspeed="300" data-endeasing="easeInSine"><a href="register.php">Register</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End slider -->

    <!-- Map box -->
    <!--	<div class="map">

			</div>

			<!-- contact box -->
    <div class="contact-box" style="margin-top:100px;">

        <div class="row">
            <form method="post" action="mail.php" enctype="multipart/form-data">
                <div class="" data-example-id="">
                    <div id="" class="" data-ride="" data-interval="">

                        <div class="" role="">
                            <div id="first" class="item active">
                                <div class="row padding10">

                                    <div class="col-md-4">
                                        <div class="contact-information">
                                            <img class="img-responsive img-circle wid300" id="blah" src="images/avatar3.png" />
                                        </div>
                                        <span>
                                            <button type="button" class="btn btn-primary mar65" id="upload">Upload Profile Picture</button>
                                            <input type='file' onchange="readURL(this);" style="visibility:hidden;" id="user_img" name="user_img"/>
                                            
                                        </span>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">First Name:</label>
                                            <input type="text" class="form-control" id="fname" name="fname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Last Name:</label>
                                            <input type="text" class="form-control" id="lname" name="lname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Company Name:</label>
                                            <input type="text" class="form-control" id="cname" name="cname">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Date Of Birth:</label>
                                            <input type="text" class="form-control" name="dob" id="dob">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Gender:</label>
                                            <label class="radio-inline">
                                                <input type="radio" id="gender" name="gender">Male</label>
                                            <label class="radio-inline">
                                                <input type="radio" id="gender" name="gender">Female</label>

                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Res. Address</label>
                                            <input type="text" class="form-control" name="residence">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Pin Code :</label>
                                            <input type="text" class="form-control" name="pin">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">City :</label>
                                            <input type="text" class="form-control" name="city">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Distt :</label>
                                            <input type="text" class="form-control" name="distt">
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pwd">State :</label>
                                            <input type="text" class="form-control" name="state">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Country :</label>
                                            <input type="text" class="form-control" name="country">
                                        </div>
                                        <div class="form-group">
                                            <label for="sel1">Indian Citizen:</label>
                                            <select class="form-control" name="citizen">
                                                <option>YES</option>
                                                <option>NO</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Mobile No. 1</label>
                                            <input type="text" class="form-control" name="mob1">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Mobile No. 2</label>
                                            <input type="text" class="form-control" name="mob2">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Landline No.</label>
                                            <input type="text" class="form-control" name="land">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Email ID</label>
                                            <input type="email" class="form-control" name="email">
                                        </div>

                                        <div id="msg" class="message"></div>
                                        <div class="top1">
                                            <a class="right carousel-control" id="next1" href="#carousel-example-generic" role="button" data-slide="next" style="right:16px;top:580px;">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
	

                                    </div>
									 

                                </div>
								  
									<div class="visible-xs">
                                            <a class="right carousel-control top4" id="next1" href="#carousel-example-generic" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
								 
                            </div>
                            <div id="second" class="item" style="display:none;">
                                <div class="row padding10">
                                    <h2 class="pad14">Location Details</h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Household Income:</label>
                                            <input type="text" class="form-control" name="house_income">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Education:</label>
                                            <input type="text" class="form-control" name="edu">
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Marital Status:</label>
                                            <input type="text" class="form-control" name="marital">
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Presently working:</label>
                                            <select class="form-control" name="p_working">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Occupation:</label>
                                            <input type="text" class="form-control" name="occu">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Additional Comments:</label>
                                            <input type="text" class="form-control" name="add_comment">
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="item" style="display:none;">
                                <div class="row padding10">

                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">SmartPhone:</label>
                                            <select class="form-control" name="smart">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Camera Resolution:</label>
                                            <select class="form-control" name="cam">
                                                <option>1px</option>
                                                <option>2px</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Laptop:</label>
                                            <select class="form-control" name="laptop">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Access to Scanner:</label>
                                            <select class="form-control" name="scanner">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sel1">Access to Printer:</label>
                                            <select class="form-control" name="printer">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="visible-xs">
										
                                            <a class="left carousel-control top5" href="#carousel-example-generic" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control top6" href="#carousel-example-generic" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
										
                                </div>

                                </div>
								
                            </div>

                            <div class="item" style="display:none;">
                                <div class="row padding10">
                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Have you ever worked as mystery shopper before :</label>
                                            <select class="form-control" name="q1">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="sel1">If yes, for which company did you work for? :</label>
                                            <select class="form-control" name="a1">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Have you even been in contact with the police or justice department? :</label>
                                            <select class="form-control" name="q2">
                                                <option>YES</option>
                                                <option>NO</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="sel1">Please upload your CV to complete your registration:</label>
                                            <input type="file" name="cv" id="cv" class="form-control" />
                                        </div>

                                        <div class="top2">
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                Back
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                Next
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="visible-xs">
                                            <a class="left carousel-control top7 top10" href="#carousel-example-generic" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control top8 top11" href="#carousel-example-generic" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>

                                </div>
                            </div>

                            <div class="item" style="display:none;">
                                <div class="row padding10">
                                    <h2 class="pad14"></h2>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Bank Account Number :</label>
                                            <input type="text" class="form-control" name="bank_detail">

                                            <div class="top3">
                                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                    Back
													<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="usr">Account holders Name :</label>
                                            <input type="text" class="form-control" name="acc_name">
                                        </div>

                                        <div class="form-group">
                                            <label for="usr">Bank Name :</label>
                                            <input type="text" class="form-control" name="bank_name">
                                        </div>

                                    </div>

                                    <div class="col-md-4" id="contact-form">

                                        <div class="form-group">
                                            <label for="usr">IFSC Code:</label>
                                            <input type="text" class="form-control" name="ifsc">
                                        </div>
                                        <div class="form-group">
                                            <label for="usr">Bank Branch Address:</label>
                                            <input type="text" class="form-control" name="bank_add">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Send" id="submit" class="btn big_white"/>
                                        </div>

            

            </div>
			 <div class="visible-xs">
                                                <a class="left carousel-control top9" href="#carousel-example-generic" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>

                                            </div>

            </div>
            </div>

            </div>

            </div>
            </div>
            </form>

        </div>
    </div>

</div>
<!-- End content -->

<!-- footer 
			================================================== -->
<?php include 'footer.php'; ?>
<!-- End footer -->

<div class="fixed-link-top">
    <div class="container">
        <a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
    </div>
</div>

</div>
<!-- End Container -->

<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
<script type="text/javascript">
    var tpj = jQuery;
    tpj.noConflict();

    tpj(document).ready(function() {

        if (tpj.fn.cssOriginal != undefined)
            tpj.fn.css = tpj.fn.cssOriginal;

        var api = tpj('.fullwidthbanner').revolution({
            delay: 100000,
            startwidth: 1170,
            startheight: 500,
            interval: false,
            onHoverStop: "on", // Stop Banner Timet at Hover on Slide on/off

            thumbWidth: 100, // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
            thumbHeight: 50,
            thumbAmount: 3,

            hideThumbs: 0,
            navigationType: "none", // bullet, thumb, none
            navigationArrows: "solo", // nexttobullets, solo (old name verticalcentered), none

            navigationStyle: "round", // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom

            navigationHAlign: "center", // Vertical Align top,center,bottom
            navigationVAlign: "bottom", // Horizontal Align left,center,right
            navigationHOffset: 30,
            navigationVOffset: 40,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 0,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 0,
            soloArrowRightVOffset: 0,

            touchenabled: "on", // Enable Swipe Function : on/off

            stopAtSlide: -1, // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops: -1, // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

            hideCaptionAtLimit: 0, // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit: 0, // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit: 0, // Hide the whole slider, and stop also functions if Width of Browser is less than this value

            fullWidth: "on",

            shadow: 1 //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

        });

        // TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
        // YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
        api.bind("revolution.slide.onloaded", function(e) {

            jQuery('.tparrows').each(function() {
                var arrows = jQuery(this);

                var timer = setInterval(function() {

                    if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
                        arrows.fadeOut(300);
                }, 3000);
            })

            jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
                jQuery('.tp-simpleresponsive').addClass("mouseisover");
                jQuery('body').find('.tparrows').each(function() {
                    jQuery(this).fadeIn(300);
                });
            }, function() {
                if (!jQuery(this).hasClass("tparrows"))
                    jQuery('.tp-simpleresponsive').removeClass("mouseisover");
            })
        });
        // END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
    });
</script>
<script>
    jQuery(function() {
        DevSolutionSkill.init('circle');
        DevSolutionSkill.init('circle2');
        DevSolutionSkill.init('circle3');
        DevSolutionSkill.init('circle4');
        DevSolutionSkill.init('circle5');
        DevSolutionSkill.init('circle6');
    });
</script>
</body>

</html>
