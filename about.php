<?php include 'header.php';
?>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>About Us</h2>
					<ul class="page-tree">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
					</ul>				
				</div>
			</div>

			<div class="fullwidth-box">
				<div class="container">
	

					<!-- about box-->
					<div class="about-box">
						<div class="row">
							<div class="col-md-4">
								<div class="about-us-text">
									<h3>Know AUDIT DADDY</h3>
									<div class="innner-box">
						<!--				<img alt="" src="upload/about.jpg"> 
							-->			<p><span> </span>India's leading Audit company working with best of the global brands across various verticals. We have head quarter in New Delhi with branch offices in all Metros. We operate PAN india with 40+ representative offices.   </p>							
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="skills-progress">
				<!--					<h3>Our Skills</h3>
				-->
				<h3>WHAT WE DO?</h3>
								<p style="font-weight:500;">A new insight system pioneered by AUDIT DADDY and shaped by some of the biggest companies in the world.
 
AUDIT DADDY helps you identify:<br>
1. At matters most to your customers<br>
2. Why your stores are performing against those expectations<br>
3. At specific improvements at which locations will drive the biggest impact to your bottom line.
									</p>
					<!--				<p>WordPress Development <span>71%</span></p>
									<div class="meter nostrips wp">
										<span style="width: 71%"></span>
									</div>
									<p>Photoshop <span>85%</span></p>
									<div class="meter nostrips photoshop">
										<span style="width: 85%"></span>
									</div>
									<p>HTML5/CSS3 <span>76%</span></p>
									<div class="meter nostrips html">
										<span style="width: 76%"></span>
									</div>
									<p>Ruby on Rails <span>53%</span></p>
									<div class="meter nostrips ruby">
										<span style="width: 53%"></span>
									</div>
									<p>Social Marketing <span>69%</span></p>
									<div class="meter nostrips marketing">
										<span style="width: 69%"></span>
									</div>
				-->				</div>
							</div>

							<div class="col-md-4">
								<div class="company-mission">
		<!--							<h3>Company Mission</h3>
										
					
					
					<span class="icon"></span>
				-->
					<h3>HOW WE DO IT?</h3>
					<p>By leveraging highly varied forms of customer data-from customer satisfaction to mystery shopping to transaction records-then layering sophisticated, targeted Analytics on top of that resulting in statistically predictive recommendations, hosted on a single, blended and intelligent technology platform. We at AuditDADDY are in total control of the entire audit execution cycle.
</p>



				
								</div>
							</div>
						</div>
					</div>

					<!-- Work Staff -->
					<!--<div class="work-staff">
						<h3>Our Staff</h3>
						<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">

							
							<div class="carousel-inner">

								<div class="item active">
									<div class="row">
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team1.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team2.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team3.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team4.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="item">
									<div class="row">
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team1.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team2.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team3.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="staff-post">
												<div class="staff-post-gal">
													<img alt="" src="upload/team4.jpg">
												</div>
												<div class="staff-post-content">
													<h5>Owen Miller</h5>
													<span>manager</span>
													<ul class="staf-social">
														<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
														<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
														<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
														<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
														<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
														<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>

								</div>

							</div>
							
							<a class="left carousel-control" href="#carousel-example-generic1" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic1" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>-->

					<!-- Client logo section -->
					<div class="client-section">
						<h3>Clients</h3>
						<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">

							<!-- Wrapper for slides -->
							<div class="carousel-inner">

								<div class="item active">
									<ul class="clients-list">
										<li><a href="#"><img alt="" src="images/1.png"></a></li>
										<li><a href="#"><img alt="" src="images/2.png"></a></li>
										<li><a href="#"><img alt="" src="images/3.png"></a></li>
										<li><a href="#"><img alt="" src="images/4.png"></a></li>
										<li><a href="#"><img alt="" src="images/5.png"></a></li>
										<li><a href="#"><img alt="" src="images/1.png"></a></li>
									</ul>
								</div>

								<div class="item">
									<ul class="clients-list">
										<li><a href="#"><img alt="" src="images/1.png"></a></li>
										<li><a href="#"><img alt="" src="images/2.png"></a></li>
										<li><a href="#"><img alt="" src="images/3.png"></a></li>
										<li><a href="#"><img alt="" src="images/4.png"></a></li>
										<li><a href="#"><img alt="" src="images/5.png"></a></li>
										<li><a href="#"><img alt="" src="images/1.png"></a></li>
									</ul>									
								</div>

							</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>

					<!-- Pricing tables -->
					<div class="pricing-box">
						<h3></h3>
						<div class="row">

							

							

							
						</div>
					</div>

				</div>
			</div>

		</div>
		<!-- End content -->


		<!-- footer 
			================================================== -->
		<?php include 'footer.php';
		?>
		<!-- End footer -->

		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>

	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</body>

</html>